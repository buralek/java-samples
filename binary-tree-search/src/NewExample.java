//Given an array of strings, group anagrams together.

//Example:

//Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
//Output:
//[
// ["ate","eat","tea"],
//  ["nat","tan"],
//  ["bat"]
//]

import java.io.*;
import java.util.*;

class NewExample {
    public static List<List<String>> groupAnagrams(String[] strs) {
        HashMap<String, List<String>> anagramMap = new HashMap<>();
        for (int i = 0; i < strs.length; i++) {
            String newString = strs[i];
            boolean newKey = true;
            for (String key : anagramMap.keySet()) {
                if (compareString(key, newString)) {
                    newKey = false;
                    List<String> anagrams = anagramMap.get(key);
                    anagrams.add(newString);
                    break;
                }
            }
            if (newKey) {
                List<String> newArray = new ArrayList<>();
                newArray.add(newString);
                anagramMap.put(newString, newArray);
            }
        }
        List<List<String>> result = new ArrayList<>();
        for (String key : anagramMap.keySet()) {
            result.add(anagramMap.get(key));
        }
        return result;

    }

    private static boolean compareString(String baseString, String newString) {
        String baseStringCopy = baseString.substring(0, baseString.length());
        for (Character c : newString.toCharArray()) {
            String charString = "" + c;
            if (!baseStringCopy.contains(charString)) {
                return false;
            }
            baseStringCopy = baseStringCopy.replaceFirst(charString, "");
        }
        return baseStringCopy.length() == 0;
    }

    public static void main(String[] args) {
        String[] input = new String[]{"eat", "tea", "tan", "ate", "nat", "bat"};
        List<List<String>> result = groupAnagrams(input);
        System.out.println(result);
    }
}

import java.io.*;
import java.util.*;

class Example {


    static void flip(int[] arr, int k) {
        for (int i = 0; i < k / 2; i++) {
            int swap = arr[i];
            arr[i] = arr[k - i - 1];
            arr[k - i - 1] = swap;
        }
    }

    static int maxElement(int[] arr, int n) {
        int max = arr[0];
        int maxNumber = 0;

        for (int i = 1; i < n; i++) {
            if (max < arr[i]) {
                max = arr[i];
                maxNumber = i;
            }
        }

        return maxNumber;
    }
  /*1, 3, 2, 5, 4

  1, 5, 4, 3, 2

    5 1 4 3 2
    2 3 4 1 5*/


    static int[] pancakeSort(int[] arr) {
        int currentMaxNumber;
        for (int i = arr.length - 1; i >= 0; i--) {
            currentMaxNumber = maxElement(arr, i);
            flip(arr, currentMaxNumber);
            flip(arr, i);
        }
        return arr;
        // your code goes here
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1, 5, 4, 3, 2};
        //flip(arr, 3);
        int[] result = pancakeSort(arr);
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i] + ", ");
        }


    }

}

package com.buralek.t360.server;

import com.buralek.t360.AbstractComponent;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.nio.channels.spi.AbstractSelector;
import java.nio.channels.spi.SelectorProvider;

public class Server extends AbstractComponent {
//objectoutputstream

    public Server(String hostname, int port) throws IOException {
        super(hostname, port);
    }

    @Override
    protected Selector createSelector() throws IOException {
        AbstractSelector selector = SelectorProvider.provider().openSelector();
        channel.register(selector, SelectionKey.OP_ACCEPT);
        return selector;
    }

    @Override
    protected AbstractSelectableChannel createChannel(InetSocketAddress address) throws IOException {
        ServerSocketChannel channel = ServerSocketChannel.open();
        channel.configureBlocking(false);
        channel.socket().bind(address);
        return channel;
    }

    @Override
    protected void handleMessage(SelectionKey senderKey, String message) throws IOException {
        for (SelectionKey key : selector.keys()) {
            if (key.channel() instanceof ServerSocketChannel) {
                continue;
            }
            if (key.equals(senderKey)) {
                continue;
            }
            System.out.println("Server: " + message);
            write(key, message);
        }
    }

    @Override
    protected void write(SelectionKey key) throws IOException, InterruptedException {
        System.out.println("Server is waiting the game start");
        Thread.sleep(500);
    }

    protected void write(SelectionKey key, String message) throws IOException {
        ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
        SocketChannel channel = (SocketChannel) key.channel();
        channel.write(buffer);
        key.interestOps(SelectionKey.OP_READ);
    }

}

package com.buralek.t360;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

import static java.nio.channels.SelectionKey.OP_READ;
import static java.nio.channels.SelectionKey.OP_WRITE;

public abstract class AbstractComponent implements Runnable {
    private final int BUFFER_SIZE = 100;
    protected Selector selector;
    protected AbstractSelectableChannel channel;
    protected InetSocketAddress address;
    protected ByteBuffer byteBuffer;
    private boolean running = true;

    public AbstractComponent(String hostname, int port) throws IOException {
        address = new InetSocketAddress(hostname, port);
        channel = createChannel(address);
        selector = createSelector();
        byteBuffer = ByteBuffer.allocate(BUFFER_SIZE);
    }

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        while (running) {
            execute();
        }
        clean();
    }

    protected void execute() {
        try {
            selector.select(1000);
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                if (!key.isValid()) {
                    continue;
                }
                if (key.isConnectable()) {
                    connect(key);
                } else if (key.isAcceptable()) {
                    accept(key);
                } else if (key.isReadable()) {
                    read(key);
                } else if (key.isWritable()) {
                    write(key);
                }
            }
        } catch (IOException | InterruptedException e) {
            System.out.println("Exception: " + e);
        }
    }

    protected void connect(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        try {
            channel.finishConnect();
            channel.configureBlocking(false);
            channel.register(selector, OP_WRITE);
        } catch (IOException e) {
            e.printStackTrace();
            key.channel().close();
            key.cancel();
        }
    }

    protected void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel socketChannel = serverSocketChannel.accept();
        socketChannel.configureBlocking(false);
        socketChannel.register(selector, OP_READ);
    }

    protected void read(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        byteBuffer.clear();
        channel.read(byteBuffer);
        String message = new String(byteBuffer.array(), StandardCharsets.UTF_8);
        handleMessage(key, message);
    }

    protected void clean() {
        try {
            for (SelectionKey key : selector.keys()) {
                key.channel().close();
            }
            selector.close();
        } catch (IOException e) {
            System.out.println("Exception: " + e);
        }
    }

    protected abstract AbstractSelectableChannel createChannel(InetSocketAddress address) throws IOException;

    protected abstract Selector createSelector() throws IOException;

    protected abstract void write(SelectionKey key) throws IOException, InterruptedException;

    protected abstract void handleMessage(SelectionKey senderKey, String message) throws IOException;
}

package com.buralek.t360.client;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public class InitiatorMessageHandler extends PlayerMessageHandler {
    private final int MAX_MESSAGES = 10;

    @Override
    public void handle(SelectionKey key, String name, String message) throws IOException {
        System.out.println(name + " client received a message: " + message);
        receivedMessagesCounter++;
        if (!stop()) {
            String newMessage = updateMessage(message);
            sendMessage(key, newMessage);
        }
        System.out.println(name + " client stopped the game");
    }

    protected boolean stop() {
        return receivedMessagesCounter >= MAX_MESSAGES && sentMessagesCounter >= MAX_MESSAGES;
    }
}

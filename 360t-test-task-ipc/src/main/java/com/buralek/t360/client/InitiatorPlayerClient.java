package com.buralek.t360.client;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

import static java.nio.channels.SelectionKey.OP_READ;

public class InitiatorPlayerClient extends PlayerClient {

    private final String FIRST_MESSAGE = "firstMessage";

    public InitiatorPlayerClient(String hostname, int port, String name, MessageHandler messageHandler) throws IOException {
        super(hostname, port, name, messageHandler);
    }

    @Override
    protected void write(SelectionKey key) throws IOException {
        System.out.println(name + " client starts the game");
        sendMessage(key, FIRST_MESSAGE);
    }

    protected String updateMessage(String oldMessage) {
        return oldMessage + "\\" + sentMessagesCounter;
    }

    protected void sendMessage(SelectionKey key, String message) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        channel.write(ByteBuffer.wrap(message.getBytes(StandardCharsets.UTF_8)));
        key.interestOps(OP_READ);
        sentMessagesCounter++;
    }
}

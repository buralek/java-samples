package com.buralek.t360.client;

import com.buralek.t360.AbstractComponent;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.nio.charset.StandardCharsets;

import static java.nio.channels.SelectionKey.OP_CONNECT;
import static java.nio.channels.SelectionKey.OP_READ;

public class PlayerClient extends AbstractComponent {
    protected String name;
    protected MessageHandler messageHandler;
    protected int sentMessagesCounter;
    protected int receivedMessagesCounter;

    public PlayerClient(String hostname, int port, String name, MessageHandler messageHandler) throws IOException {
        super(hostname, port);
        this.name = name;
        this.messageHandler = messageHandler;
        sentMessagesCounter = 0;
        receivedMessagesCounter = 0;
    }

    @Override
    protected AbstractSelectableChannel createChannel(InetSocketAddress address) throws IOException {
        SocketChannel channel = SocketChannel.open();
        channel.configureBlocking(false);
        channel.connect(address);
        return channel;
    }

    @Override
    protected Selector createSelector() throws IOException {
        Selector selector = Selector.open();
        channel.register(selector, OP_CONNECT);
        return selector;
    }

    @Override
    protected void handleMessage(SelectionKey senderKey, String message) throws IOException {
        System.out.println(name + " client received a message: " + message);
        receivedMessagesCounter++;
        String newMessage = updateMessage(message);
        sendMessage(senderKey, newMessage);
    }

    @Override
    protected void write(SelectionKey key) throws IOException, InterruptedException {
        System.out.println(name + " client is waiting the game start");
        Thread.sleep(500);
        key.interestOps(OP_READ);
    }

    protected String updateMessage(String oldMessage) {
        return oldMessage + "\\" + sentMessagesCounter;
    }

    protected void sendMessage(SelectionKey key, String message) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        channel.write(ByteBuffer.wrap(message.getBytes(StandardCharsets.UTF_8)));
        key.interestOps(OP_READ);
        sentMessagesCounter++;
    }
}

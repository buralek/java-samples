package com.buralek.t360.client;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

import static java.nio.channels.SelectionKey.OP_READ;

public class PlayerMessageHandler implements MessageHandler {
    protected int sentMessagesCounter;
    protected int receivedMessagesCounter;

    public PlayerMessageHandler() {
        sentMessagesCounter = 0;
        receivedMessagesCounter = 0;
    }

    @Override
    public void handle(SelectionKey key, String name, String message) throws IOException {
        System.out.println(name + " client received a message: " + message);
        receivedMessagesCounter++;
        String newMessage = updateMessage(message);
        sendMessage(key, newMessage);
    }

    protected String updateMessage(String oldMessage) {
        return oldMessage + "\\" + sentMessagesCounter;
    }

    protected void sendMessage(SelectionKey key, String message) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        channel.write(ByteBuffer.wrap(message.getBytes(StandardCharsets.UTF_8)));
        key.interestOps(OP_READ);
        sentMessagesCounter++;
    }
}

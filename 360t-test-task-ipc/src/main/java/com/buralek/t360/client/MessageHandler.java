package com.buralek.t360.client;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public interface MessageHandler {
    void handle(SelectionKey senderKey, String name, String message) throws IOException;
}

package com.buralek.t360;

import com.buralek.t360.client.InitiatorMessageHandler;
import com.buralek.t360.client.InitiatorPlayerClient;
import com.buralek.t360.client.MessageHandler;
import com.buralek.t360.client.PlayerClient;
import com.buralek.t360.client.PlayerMessageHandler;
import com.buralek.t360.server.Server;

import java.io.IOException;

public class StartApp {
    public static void main(String[] args) {
        try {
            Server server = new Server("localhost", 5555);
            server.start();

            MessageHandler initiatorMessageHandler = new InitiatorMessageHandler();
            PlayerClient player1 = new InitiatorPlayerClient("localhost", 5555, "Player1", initiatorMessageHandler);
            player1.start();

            MessageHandler playerMessageHandler = new PlayerMessageHandler();
            PlayerClient player2 = new PlayerClient("localhost", 5555, "Player2", playerMessageHandler);
            player2.start();

            Thread.sleep(1000);
        } catch (IOException | InterruptedException e) {
            System.out.println("Exception: " + e);
        }

    }
}

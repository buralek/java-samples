import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Waiter implements Runnable {
    OrderQueue orderQueue;
    private int currentCustomerId;
    private int currentDishName;

    private final int ORDERS_NUMBER = 10;
    private final List<String> dishNames = createDishNames();

    public Waiter(OrderQueue orderQueue) {
        this.orderQueue = orderQueue;
    }

    @Override
    public void run() {
        synchronized (orderQueue) {
            try {

            }
            for (int i = 1; i <= ORDERS_NUMBER; i++) {
                orderQueue.addOrder((int) ((Math.random() + 1) * 5), dishNames.get((int) Math.random() * 2));
                Thread.sleep(1000);

            }
        }
    }

    public int getCurrentCustomerId() {
        return currentCustomerId;
    }

    public void setCurrentCustomerId(int currentCustomerId) {
        this.currentCustomerId = currentCustomerId;
    }

    public int getCurrentDishName() {
        return currentDishName;
    }

    public void setCurrentDishName(int currentDishName) {
        this.currentDishName = currentDishName;
    }

    private List<String> createDishNames() {
        List<String> dishNames = new ArrayList<>();
        dishNames.add("Salad");
        dishNames.add("Pizza");
        dishNames.add("Pasta");
        return dishNames;
    }
}

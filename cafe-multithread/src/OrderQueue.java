import java.util.List;

public class OrderQueue {
    private List<Order> orders;

    public synchronized void addOrder(int customerId, String dishName) {
        Order currentOrder = new Order();
        currentOrder.setId(orders.size() + 1);
        currentOrder.setCustomerId(customerId);
        currentOrder.setDishName(dishName);
        orders.add(currentOrder);
    }

    public synchronized Order removeOrder() {
        return orders.stream().findFirst().orElseThrow(() -> new RuntimeException("Queue is empty!"));
    }
}

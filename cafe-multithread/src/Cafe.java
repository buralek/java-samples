public class Cafe {
    private OrderQueue orderQueue;
    private Waiter waiter;
    private Cook cook;

    public void start() {
        if (waiter == null || cook == null) {
            throw new RuntimeException("Can't open the cafe without staff");
        }
        Thread waiterThread = new Thread(waiter);
        Thread cookThread = new Thread(cook);
        waiterThread.start();
        cookThread.start();

    }

    public Waiter getWaiter() {
        return waiter;
    }

    public void setWaiter(Waiter waiter) {
        this.waiter = waiter;
    }

    public Cook getCook() {
        return cook;
    }

    public void setCook(Cook cook) {
        this.cook = cook;
    }

    public OrderQueue getOrderQueue() {
        return orderQueue;
    }

    public void setOrderQueue(OrderQueue orderQueue) {
        this.orderQueue = orderQueue;
    }
}

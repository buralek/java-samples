# 360T Task

## Task description

Having a Player class - an instance of this class with that can communicate with other Player(s) (other instances of this class)

The use case for this task is as bellow:

1. create 2 players

2. one of the players should send a message to second player (let's call this player "initiator")

3. when a player receives a message should send back a new message that contains the received message concatenated with the message counter that this player sent.

4. finalize the program (gracefully) after the initiator sent 10 messages and received back 10 messages (stop condition)

5. both players should run in the same java process (strong requirement)

6. document for every class the responsibilities it has.

7. opposite to 5: have every player in a separate JAVA process (different PID).

Please use pure Java as much as possible (no additional frameworks like spring, etc.)
Please deliver one single maven project with the source code only (no jars). Please send the maven project as archive attached to e-mail (eventual links for download will be ignored due to security policy).
Please provide a shell script to start the program.
Everything what is not clearly specified is to be decided by developer. Everything what is specified is a hard requirement.
Please focus on design and not on technology, the technology should be the simplest possible that is achieving the target.
The focus of the exercise is to deliver the cleanest and clearest design that you can achieve (and the system has to be functional).

## Installation



```bash
./start.sh
```

## Usage
You can chose one of these 3 solutions or stop the program.

## Solutions
There are 3 different solutions
1. **Simple single thread game with StringBuilder realization.**
   It contains 2 players and shared StringBuilder.
   The initializer starts the game and then both players moves one after another
2. **Single thread game with an Observer pattern.**
   There is a gameEventManager with eventQueue and 2 players which are subscribed on different events.
   The initializer starts the game. The gameEventManager notifies subscribed player. This player updates the message and sends new event to the gameEventManager.
   Repeat until the initializer doesn't stop the game.
3. **Multi thread solution.**
   There is a shared Queue with gameEvents. Main thread starts 2 players threads and starts to wait.
   The second player thread is waiting until the initializer starts the game(puts message to the shared Queue).
   Then players exchange events through the shared Queue.
   When the initializer wants to stop the game, he sends STOP_EVENT and stops his own thread. The second player receives STOP_EVENT and stops the thread too.
   Main thread continues and the user can chose another game.

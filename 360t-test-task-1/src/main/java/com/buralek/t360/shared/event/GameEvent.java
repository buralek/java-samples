package com.buralek.t360.shared.event;

/**
 * Game event which is used in the single thread and multi thread solutions
 */
public class GameEvent {
    private String eventType;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}

package com.buralek.t360.shared;

/**
 * This interface initialize and start a game
 */
public interface Game {
    /**
     * Initializes and starts the game
     *
     * @param firstMessage an initiator will use this message to start a game
     */
    void start(String firstMessage);
}

package com.buralek.t360.shared.event;

/**
 * Factory creates game events
 *
 * @param <T> create event which extends <code>GameEvent</code>
 */
public interface GameEventFactory<T extends GameEvent> {
    /**
     * Create a game event
     *
     * @param eventType
     * @param message
     * @return created game event
     */
    T create(String eventType, String message);
}

package com.buralek.t360;

import com.buralek.t360.multithread.MultiThreadGameImpl;
import com.buralek.t360.shared.Game;
import com.buralek.t360.simple.SimpleGameImpl;
import com.buralek.t360.singlethread.SingleThreadGameImpl;

import java.io.InputStreamReader;
import java.util.Scanner;

public class StartApp {
    private static final String FIRST_MESSAGE = "FirstMessage";

    public static void main(String[] args) {
        Game game;
        while (true) {
            game = null;
            System.out.println("Please, choose the game type:\n" +
                    "1) Simple game with StringBuilder realization\n" +
                    "2) Single thread game with an Observer pattern\n" +
                    "3) Multi thread game with 2 threads\n" +
                    "4) Exit\n" +
                    "Enter the number:");
            InputStreamReader inputReader = new InputStreamReader(System.in);
            Scanner inputScanner = new Scanner(inputReader);
            String gameType = inputScanner.next();
            switch (gameType) {
                case "1":
                    game = new SimpleGameImpl();
                    break;
                case "2":
                    game = new SingleThreadGameImpl();
                    break;
                case "3":
                    game = new MultiThreadGameImpl();
                    break;
                case "4":
                    System.exit(0);
                default:
                    System.out.println("We can't start game with type " + gameType);

            }
            if (game != null) {
                game.start(FIRST_MESSAGE);
            }
        }
    }
}

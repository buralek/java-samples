package com.buralek.t360.simple.utils;

/**
 * <code>SimpleGameUtil</code> realization
 */
public class SimpleGameUtilImpl implements SimpleGameUtil {
    private final int MAX_MESSAGES = 10;

    public boolean continueGame(int sentMessageCounter, int receiveMessageCounter) {
        return sentMessageCounter < MAX_MESSAGES || receiveMessageCounter < MAX_MESSAGES;
    }
}

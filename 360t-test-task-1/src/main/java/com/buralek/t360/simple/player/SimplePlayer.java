package com.buralek.t360.simple.player;

/**
 * Player interface for simple solution
 */
public interface SimplePlayer {
    /**
     * Starts a game with firstMessage
     *
     * @param firstMessage
     */
    void initGame(String firstMessage);

    /**
     * Does a player move
     */
    void doAction();

    int getReceivedMessagesCounter();

    int getSentMessagesCounter();

    String getName();
}

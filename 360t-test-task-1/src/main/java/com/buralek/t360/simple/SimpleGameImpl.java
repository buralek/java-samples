package com.buralek.t360.simple;

import com.buralek.t360.shared.Game;
import com.buralek.t360.simple.player.SimplePlayer;
import com.buralek.t360.simple.player.SimplePlayerImpl;
import com.buralek.t360.simple.utils.SimpleGameUtil;
import com.buralek.t360.simple.utils.SimpleGameUtilImpl;

/**
 * Simple realization with shared <code>StringBuilder</>
 * 1) Create shared messageBuilder
 * 2) First player inits the game(put first message to messageBuilder)
 * 3) Start a loop and repeat it until stop game condition will be true
 */
public class SimpleGameImpl implements Game {
    private SimplePlayer firstPlayer;
    private SimplePlayer secondPlayer;
    private StringBuilder messageBuilder;
    private SimpleGameUtil simpleGameUtil;

    public SimpleGameImpl() {
        messageBuilder = new StringBuilder();
        simpleGameUtil = new SimpleGameUtilImpl();
        firstPlayer = new SimplePlayerImpl("Player1", messageBuilder);
        secondPlayer = new SimplePlayerImpl("Player2", messageBuilder);
    }

    @Override
    public void start(String firstMessage) {
        System.out.println("Start a simple game");
        firstPlayer.initGame(firstMessage);
        while (simpleGameUtil.continueGame(firstPlayer.getSentMessagesCounter(), firstPlayer.getReceivedMessagesCounter())) {
            secondPlayer.doAction();
            firstPlayer.doAction();
        }
        System.out.println(firstPlayer.getName() + " stopped the game. Sent messages = " + firstPlayer.getSentMessagesCounter() + " and received messages = " + firstPlayer.getReceivedMessagesCounter());
    }
}

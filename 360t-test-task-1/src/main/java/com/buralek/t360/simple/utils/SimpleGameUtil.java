package com.buralek.t360.simple.utils;

/**
 * This class contains help methods for simple game realization
 */
public interface SimpleGameUtil {
    /**
     * Checks end game condition
     *
     * @param sentMessageCounter
     * @param receiveMessageCounter
     * @return false if game must be stopped
     */
    boolean continueGame(int sentMessageCounter, int receiveMessageCounter);
}

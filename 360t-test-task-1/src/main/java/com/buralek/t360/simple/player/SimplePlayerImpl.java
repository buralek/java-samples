package com.buralek.t360.simple.player;

import com.buralek.t360.simple.utils.SimpleGameUtil;
import com.buralek.t360.simple.utils.SimpleGameUtilImpl;

/**
 * Player realization for simple solution
 * Player can get current game string from messageBuilder and put new message to the messageBuilder
 * Player use <code>SimpleGameUtil</> for checking stop condition
 */
public class SimplePlayerImpl implements SimplePlayer {
    private String name;
    private int sentMessagesCounter;
    private int receivedMessagesCounter;
    private StringBuilder messageBuilder;
    private SimpleGameUtil simpleGameUtil;

    public SimplePlayerImpl(String name, StringBuilder messageBuilder) {
        this.name = name;
        this.messageBuilder = messageBuilder;
        simpleGameUtil = new SimpleGameUtilImpl();
        sentMessagesCounter = 0;
        receivedMessagesCounter = 0;
    }

    @Override
    public void initGame(String firstMessage) {
        System.out.println(name + " move: " + firstMessage);
        messageBuilder.append(firstMessage);
        sentMessagesCounter++;
    }

    /**
     * 1) Update player's receivedMessagesCounter
     * 2) If the player can continue the game, the player adds new message to StringBuilder
     * 3) Update player's sentMessagesCounter
     */
    @Override
    public void doAction() {
        receivedMessagesCounter++;
        if (simpleGameUtil.continueGame(sentMessagesCounter, receivedMessagesCounter)) {
            messageBuilder.append("\\").append(sentMessagesCounter);
            System.out.println(name + " move: " + messageBuilder.toString());
            sentMessagesCounter++;
        }
    }

    @Override
    public int getReceivedMessagesCounter() {
        return receivedMessagesCounter;
    }

    @Override
    public int getSentMessagesCounter() {
        return sentMessagesCounter;
    }

    @Override
    public String getName() {
        return name;
    }
}

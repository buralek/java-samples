package com.buralek.t360.singlethread.player;

import com.buralek.t360.shared.event.GameEvent;
import com.buralek.t360.shared.event.GameEventFactory;
import com.buralek.t360.shared.event.GameEventFactoryImpl;
import com.buralek.t360.singlethread.manager.GameEventManager;

public class SingleThreadPlayerImpl implements SingleThreadPlayer {
    private final int MAX_MESSAGES = 10;

    private String name;
    private String playerEventType;
    private boolean initializer;
    private int sentMessagesCounter;
    private int receivedMessagesCounter;
    private GameEventManager gameEventManager;
    private GameEventFactory gameEventFactory;

    public SingleThreadPlayerImpl(String name, String playerEventType, boolean initializer, GameEventManager gameEventManager) {
        this.name = name;
        this.playerEventType = playerEventType;
        this.initializer = initializer;
        this.gameEventManager = gameEventManager;
        gameEventFactory = new GameEventFactoryImpl();
        sentMessagesCounter = 0;
        receivedMessagesCounter = 0;
    }

    /**
     * 1) Creates new gameEvent with firstMessage
     * 2) Updates sentMessagesCounter
     * 2) Sends the event to gameEventManager
     *
     * @param firstMessage
     */
    @Override
    public void initGame(String firstMessage) {
        System.out.println(name + " move: " + firstMessage);
        GameEvent newGameEvent = gameEventFactory.create(playerEventType, firstMessage);
        sentMessagesCounter++;
        gameEventManager.putGameEvent(newGameEvent);
    }

    /**
     * 1) Updates receivedMessagesCounter
     * 2) If the player can continue the game, the player creates new gameEvent and updates this with received message
     * 3) Sends the event to gameEventManager
     *
     * @param message received message
     */
    @Override
    public void doAction(String message) {
        receivedMessagesCounter++;
        if (endGame()) {
            System.out.println(name + " stopped the game. Sent messages = " + sentMessagesCounter + " and received messages = " + receivedMessagesCounter);
        } else {
            String newMessage = updateMessage(message);
            GameEvent newGameEvent = gameEventFactory.create(playerEventType, newMessage);
            System.out.println(name + " move: " + newMessage);
            sentMessagesCounter++;
            gameEventManager.putGameEvent(newGameEvent);
        }
    }

    /**
     * Updates the message regarding the rule
     *
     * @param oldMessage message which will be updated
     * @return new updated message
     */
    protected String updateMessage(String oldMessage) {
        return oldMessage + "\\" + sentMessagesCounter;
    }

    /**
     * Checks the end game condition
     *
     * @return true if game must be stopped
     */
    protected boolean endGame() {
        return initializer && receivedMessagesCounter >= MAX_MESSAGES && sentMessagesCounter >= MAX_MESSAGES;
    }
}

package com.buralek.t360.singlethread.manager;

import com.buralek.t360.shared.event.GameEvent;
import com.buralek.t360.singlethread.player.SingleThreadPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * <code>GameEventManager</> realization
 */
public class GameEventManagerImpl implements GameEventManager {
    private Map<String, List<SingleThreadPlayer>> playerObservers = new HashMap<>();
    private Queue<GameEvent> gameEvents;

    public GameEventManagerImpl(String... eventTypes) {
        for (String eventType : eventTypes) {
            playerObservers.put(eventType, new ArrayList<>());
            gameEvents = new LinkedList<>();
        }
    }

    @Override
    public void subscribe(String eventType, SingleThreadPlayer player) {
        List<SingleThreadPlayer> players = playerObservers.get(eventType);
        players.add(player);
    }

    @Override
    public void unsubscribe(String gameEvent, SingleThreadPlayer player) {
        List<SingleThreadPlayer> playerImpls = playerObservers.get(gameEvent);
        playerImpls.remove(player);
    }

    @Override
    public void putGameEvent(GameEvent gameEvent) {
        updateGameEvents(gameEvent);
        notifyPlayers(gameEvent.getEventType());
    }

    /**
     * Puts new event to gameEvents queue
     *
     * @param gameEvent new event
     */
    protected void updateGameEvents(GameEvent gameEvent) {
        gameEvents.add(gameEvent);
    }

    /**
     * Notifies all subscribed players about new event with eventType
     *
     * @param eventType notified event type
     */
    protected void notifyPlayers(String eventType) {
        List<SingleThreadPlayer> playerImpls = playerObservers.get(eventType);
        GameEvent gameEvent = gameEvents.poll();
        playerImpls.forEach(player -> player.doAction(gameEvent.getMessage()));
    }

}

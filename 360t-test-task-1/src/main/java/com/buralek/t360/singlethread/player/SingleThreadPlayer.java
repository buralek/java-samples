package com.buralek.t360.singlethread.player;

/**
 * Player interface for single thread solution
 */
public interface SingleThreadPlayer {
    /**
     * Starts a game with firstMessage
     *
     * @param firstMessage
     */
    void initGame(String firstMessage);

    /**
     * Does the player move
     *
     * @param message
     */
    void doAction(String message);
}

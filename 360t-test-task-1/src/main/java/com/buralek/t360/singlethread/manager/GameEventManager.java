package com.buralek.t360.singlethread.manager;

import com.buralek.t360.shared.event.GameEvent;
import com.buralek.t360.singlethread.player.SingleThreadPlayer;

/**
 * Single thread game event manager
 */
public interface GameEventManager {
    /**
     * Subscribes a chosen player to game events with eventType
     *
     * @param eventType a subscribed event type
     * @param player    a subscribed player
     */
    void subscribe(String eventType, SingleThreadPlayer player);

    /**
     * Unsubscribes a chosen player to game events with eventType
     *
     * @param eventType an unsubscribed event type
     * @param player    an unsubscribed player
     */
    void unsubscribe(String eventType, SingleThreadPlayer player);

    /**
     * Put new game event to the game manager
     *
     * @param gameEvent new game event
     */
    void putGameEvent(GameEvent gameEvent);
}

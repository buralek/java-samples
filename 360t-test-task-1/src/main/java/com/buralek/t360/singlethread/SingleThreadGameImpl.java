package com.buralek.t360.singlethread;

import com.buralek.t360.shared.Game;
import com.buralek.t360.shared.event.EventTypes;
import com.buralek.t360.singlethread.manager.GameEventManager;
import com.buralek.t360.singlethread.manager.GameEventManagerImpl;
import com.buralek.t360.singlethread.player.SingleThreadPlayer;
import com.buralek.t360.singlethread.player.SingleThreadPlayerImpl;

/**
 * Single thread game realization with Observer pattern
 * 1) Creates gameEventManager
 * 2) Subscribes players
 * 3) Init a game
 * 4) GameEventManager manages and stops the game
 * 5) Unsubscribes players
 */
public class SingleThreadGameImpl implements Game {
    private SingleThreadPlayer firstPlayer;
    private SingleThreadPlayer secondPlayer;
    private GameEventManager gameEventManager;

    public SingleThreadGameImpl() {
        gameEventManager = new GameEventManagerImpl(EventTypes.PLAYER_1_EVENT, EventTypes.PLAYER_2_EVENT);
        firstPlayer = new SingleThreadPlayerImpl("Player1", EventTypes.PLAYER_1_EVENT, true, gameEventManager);
        secondPlayer = new SingleThreadPlayerImpl("Player2", EventTypes.PLAYER_2_EVENT, false, gameEventManager);
    }

    @Override
    public void start(String firstMessage) {
        System.out.println("Start a single thread game");
        gameEventManager.subscribe(EventTypes.PLAYER_1_EVENT, secondPlayer);
        gameEventManager.subscribe(EventTypes.PLAYER_2_EVENT, firstPlayer);
        firstPlayer.initGame(firstMessage);
        gameEventManager.unsubscribe(EventTypes.PLAYER_1_EVENT, secondPlayer);
        gameEventManager.unsubscribe(EventTypes.PLAYER_2_EVENT, firstPlayer);
    }
}

package com.buralek.t360.multithread.player;


import com.buralek.t360.shared.event.EventTypes;
import com.buralek.t360.shared.event.GameEvent;
import com.buralek.t360.shared.event.GameEventFactory;
import com.buralek.t360.shared.event.GameEventFactoryImpl;

import java.util.Queue;

/**
 * Player realization for multi thread solution
 * Each player have subscribedEventType for receiving appropriate messages and playerEventType for sending messages
 */
public class MultiThreadPlayer implements Runnable {
    private final int MAX_MESSAGES = 10;

    private String name;
    private final String playerEventType;
    private final String subscribedEventType;
    private boolean initializer;
    private String firstMessage;
    private int sentMessagesCounter;
    private int receivedMessagesCounter;
    private Queue<GameEvent> gameEvents;
    private GameEventFactory gameEventFactory;

    public MultiThreadPlayer(String name, String playerEventType, String subscribedEventType, boolean initializer, Queue<GameEvent> gameEvents) {
        this.name = name;
        this.playerEventType = playerEventType;
        this.subscribedEventType = subscribedEventType;
        this.initializer = initializer;
        this.gameEvents = gameEvents;
        gameEventFactory = new GameEventFactoryImpl();
        sentMessagesCounter = 0;
        receivedMessagesCounter = 0;
    }

    @Override
    public void run() {
        try {
            // The initializer starts the game, others players are waiting the start
            synchronized (gameEvents) {
                if (initializer) {
                    initGame(firstMessage);
                    gameEvents.notifyAll();
                } else {
                    while (gameEvents.isEmpty()) {
                        gameEvents.wait(1000);
                    }
                }
            }

            // A player makes the action or stops if the initializer has stopped the game
            while (!endGame()) {
                synchronized (gameEvents) {
                    GameEvent gameEvent = gameEvents.peek();
                    String eventType = gameEvent.getEventType();
                    if (eventType.equals(EventTypes.STOP_EVENT)) {
                        System.out.println(name + " stopped the game because of stop event");
                        break;
                    }
                    if (eventType.equals(subscribedEventType)) {
                        doAction();
                        gameEvents.notifyAll();
                    } else {
                        while (gameEvents.isEmpty()) {
                            gameEvents.wait(1000);
                        }
                    }
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Exception: " + e);
        } finally {
            // The initializer creates a stop event for others players
            if (initializer) {
                synchronized (gameEvents) {
                    try {
                        sendStopEvent();
                    } catch (InterruptedException e) {
                        System.out.println("Exception: " + e);
                    }
                }
            }
        }

    }

    /**
     * Sends the first game event with firstMessage to the queue and starts a game
     *
     * @param firstMessage first message in a game
     * @throws InterruptedException
     */
    protected void initGame(String firstMessage) throws InterruptedException {
        Thread.sleep(100);
        System.out.println(name + "/PID" + Thread.currentThread().getId() + " move: " + firstMessage);
        GameEvent newGameEvent = gameEventFactory.create(playerEventType, firstMessage);
        sentMessagesCounter++;
        gameEvents.add(newGameEvent);
    }

    /**
     * 1) Gets gameEvent from the queue
     * 2) Increments receivedMessagesCounter
     * 3) Updates the message
     * 4) Sends the message to the queue
     *
     * @throws InterruptedException
     */
    protected void doAction() throws InterruptedException {
        Thread.sleep(100);
        GameEvent gameEvent = gameEvents.poll();
        receivedMessagesCounter++;
        if (!endGame()) {
            String newMessage = updateMessage(gameEvent.getMessage());
            GameEvent newGameEvent = gameEventFactory.create(playerEventType, newMessage);
            System.out.println(name + "/PID" + Thread.currentThread().getId() + " move: " + newMessage);
            sentMessagesCounter++;
            gameEvents.add(newGameEvent);
        }
    }

    /**
     * The initializer needs to notify others players that he has stopped the game.
     * The initializer send stop game event to the queue.
     *
     * @throws InterruptedException
     */
    protected void sendStopEvent() throws InterruptedException {
        Thread.sleep(100);
        System.out.println(name + " stopped the game because of counters. Sent messages = " + sentMessagesCounter + " and received messages = " + receivedMessagesCounter);
        GameEvent stopEvent = gameEventFactory.create(EventTypes.STOP_EVENT, "");
        gameEvents.add(stopEvent);
    }

    /**
     * Updates the message regarding the rule
     *
     * @param oldMessage message which will be updated
     * @return new updated message
     */
    protected String updateMessage(String oldMessage) {
        return oldMessage + "\\" + sentMessagesCounter;
    }

    /**
     * Checks the end game condition
     *
     * @return true if game must be stopped
     */
    protected boolean endGame() {
        return initializer && receivedMessagesCounter >= MAX_MESSAGES && sentMessagesCounter >= MAX_MESSAGES;
    }

    public void setFirstMessage(String firstMessage) {
        this.firstMessage = firstMessage;
    }
}

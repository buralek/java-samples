package com.buralek.t360.multithread;

import com.buralek.t360.multithread.player.MultiThreadPlayer;
import com.buralek.t360.shared.Game;
import com.buralek.t360.shared.event.EventTypes;
import com.buralek.t360.shared.event.GameEvent;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Multi thread game realization
 * Both players send messages to a shared gameEvents queue until the initializer stops the game and send STOP_EVENT to others players
 * Main thread continues when both threads finish
 */
public class MultiThreadGameImpl implements Game {
    private Queue<GameEvent> gameEvents;
    private MultiThreadPlayer firstPlayer;
    private MultiThreadPlayer secondPlayer;

    public MultiThreadGameImpl() {
        gameEvents = new LinkedList<>();
        firstPlayer = new MultiThreadPlayer("Player1", EventTypes.PLAYER_1_EVENT, EventTypes.PLAYER_2_EVENT, true, gameEvents);
        secondPlayer = new MultiThreadPlayer("Player2", EventTypes.PLAYER_2_EVENT, EventTypes.PLAYER_1_EVENT, false, gameEvents);
    }

    @Override
    public void start(String firstMessage) {
        firstPlayer.setFirstMessage(firstMessage);
        Thread firstPlayerThread = new Thread(firstPlayer);
        Thread secondPlayerThread = new Thread(secondPlayer);
        try {
            firstPlayerThread.start();
            secondPlayerThread.start();
            firstPlayerThread.join();
            secondPlayerThread.join();
        } catch (InterruptedException e) {
            System.out.println("Exception: " + e);
        }

    }
}

package com.buralek.oauthresourceserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    private final String HELLO_PATH = "/hello";

    @RequestMapping(method = RequestMethod.GET, path = HELLO_PATH)
    public String getHello() {
        return "Hello";
    }
}

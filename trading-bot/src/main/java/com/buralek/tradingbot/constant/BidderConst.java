package com.buralek.tradingbot.constant;

public abstract class BidderConst {
    public static final int LAST_ENEMY_STEPS_NUMBER = 3;
    public static final int EMPTY_ENEMY_BID = -1;
}

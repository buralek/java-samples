package com.buralek.tradingbot.constant;

public abstract class AuctionUtilConst {
    public static final String BIDDER_1_NAME = "Bidder1";
    public static final String BIDDER_2_NAME = "Bidder2";
    public static final String EQUALITY_NAME = "Equality";
}

package com.buralek.tradingbot.constant;

public abstract class BidderCoefficient {
    public static final double LOW_DIF_QUANTITY_COEFFICIENT = 1;
    public static final double MEDIUM_DIF_QUANTITY_COEFFICIENT = 1.5;
    public static final double HIGH_DIF_QUANTITY_COEFFICIENT = 2;

    public static final double EQUALITY_QUANTITY_COEFFICIENT = 0.05;

    public static final double AVERAGE_PREDICTED_ENEMY_BID_COEFFICIENT = 1.8;

    public static final double BID_ACCORDING_PREDICTED_ENEMY_BID_COEFFICIENT = 1.2;

    public static final double RANDOM_COEFFICIENT = 0.2;

    public static final double ENEMY_INCREASE_LOST_BID = 1.2;
}


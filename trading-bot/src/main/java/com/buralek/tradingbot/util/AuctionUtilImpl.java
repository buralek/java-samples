package com.buralek.tradingbot.util;

import com.buralek.tradingbot.constant.AuctionUtilConst;

import java.util.logging.Logger;

public class AuctionUtilImpl implements AuctionUtil {
    private final Logger log = Logger.getLogger(AuctionUtilImpl.class.getName());

    private int quantity;
    private int cashStart;

    private int bidder1Cash;
    private int bidder1Quantity;

    private int bidder2Cash;
    private int bidder2Quantity;

    public AuctionUtilImpl(int quantity, int cashStart) {
        this.quantity = quantity;
        this.cashStart = cashStart;
        this.bidder1Cash = cashStart;
        this.bidder1Quantity = 0;
        this.bidder2Cash = cashStart;
        this.bidder2Quantity = 0;
    }

    @Override
    public String finishCurrentAuction(int bid1, int bid2) {
        String winner;
        if (bid1 > bid2) {
            bidder1Quantity += 2;
            winner = AuctionUtilConst.BIDDER_1_NAME;
        } else if (bid1 == bid2) {
            bidder1Quantity++;
            bidder2Quantity++;
            winner = AuctionUtilConst.EQUALITY_NAME;
        } else {
            bidder2Quantity += 2;
            winner = AuctionUtilConst.BIDDER_2_NAME;
        }
        quantity -= 2;
        bidder1Cash -= bid1;
        bidder2Cash -= bid2;
        log.info("Remained quantity = " + quantity);
        log.info("Bidder1 cash = " + bidder1Cash);
        log.info("Bidder1 quantity = " + bidder1Quantity);
        log.info("Bidder2 cash = " + bidder2Cash);
        log.info("Bidder2 quantity = " + bidder2Quantity);
        log.info("Winner is " + winner);
        log.info("-------------------------------------------");
        return winner;
    }

    @Override
    public String result() {
        if (bidder1Quantity > bidder2Quantity) {
            return AuctionUtilConst.BIDDER_1_NAME;
        } else if (bidder1Quantity == bidder2Quantity) {
            return AuctionUtilConst.EQUALITY_NAME;
        } else {
            return AuctionUtilConst.BIDDER_2_NAME;
        }
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int getCashStart() {
        return cashStart;
    }

    @Override
    public void setCashStart(int cashStart) {
        this.cashStart = cashStart;
    }

    @Override
    public int getBidder1Cash() {
        return bidder1Cash;
    }

    @Override
    public void setBidder1Cash(int bidder1Cash) {
        this.bidder1Cash = bidder1Cash;
    }

    @Override
    public int getBidder1Quantity() {
        return bidder1Quantity;
    }

    @Override
    public void setBidder1Quantity(int bidder1Quantity) {
        this.bidder1Quantity = bidder1Quantity;
    }

    @Override
    public int getBidder2Cash() {
        return bidder2Cash;
    }

    @Override
    public void setBidder2Cash(int bidder2Cash) {
        this.bidder2Cash = bidder2Cash;
    }

    @Override
    public int getBidder2Quantity() {
        return bidder2Quantity;
    }

    @Override
    public void setBidder2Quantity(int bidder2Quantity) {
        this.bidder2Quantity = bidder2Quantity;
    }
}

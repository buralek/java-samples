package com.buralek.tradingbot.util;

public interface AuctionUtil {
    String finishCurrentAuction(int bid1, int bid2);

    String result();

    int getQuantity();

    void setQuantity(int quantity);

    int getCashStart();

    void setCashStart(int cashStart);

    int getBidder1Cash();

    void setBidder1Cash(int bidder1Cash);

    int getBidder1Quantity();

    void setBidder1Quantity(int bidder1Quantity);

    int getBidder2Cash();

    void setBidder2Cash(int bidder2Cash);

    int getBidder2Quantity();

    void setBidder2Quantity(int bidder2Quantity);
}

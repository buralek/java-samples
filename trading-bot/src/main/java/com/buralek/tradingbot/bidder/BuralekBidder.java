package com.buralek.tradingbot.bidder;

import com.buralek.tradingbot.constant.BidderCoefficient;
import com.buralek.tradingbot.constant.BidderConst;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.math3.analysis.interpolation.AkimaSplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.apache.commons.math3.distribution.NormalDistribution;

import java.util.Queue;
import java.util.logging.Logger;

public class BuralekBidder implements Bidder {
    private Logger log = Logger.getLogger(BuralekBidder.class.getName());

    protected double quantity;

    protected double myCash;
    protected double myQuantity;
    protected boolean lastGameWin;
    protected double myLastBid;

    protected double enemyCash;
    protected double enemyQuantity;
    protected Queue<Integer> enemyLastBids;
    protected int numberNonEmptyLastBids;

    @Override
    public void init(int quantity, int cash) {
        this.quantity = quantity;

        this.myCash = cash;
        this.myQuantity = 0;
        this.myLastBid = 0;
        this.lastGameWin = false;

        this.enemyCash = cash;
        this.enemyQuantity = 0;
        this.enemyLastBids = new CircularFifoQueue<>(BidderConst.LAST_ENEMY_STEPS_NUMBER);
        this.numberNonEmptyLastBids = 0;
    }

    @Override
    public int placeBid() {
        double myBid = handleGeneralParametersStep();
        log.info("MyBid = " + myBid);

        double predictedEnemyBid = predictEnemyBidStep();
        log.info("PredictedEnemyBid = " + predictedEnemyBid);

        myBid = handleBidAccordingEnemyPredictedBidStep(myBid, predictedEnemyBid);
        log.info("My handled bid according enemy predicted bid = " + myBid);

        myBid = useRandomStep(myBid);
        log.info("My bid after random move = " + myBid);

        int myResultedBid = checkBidStep(myBid);
        log.info("My resulted bid = " + myResultedBid);
        return myResultedBid;
    }

    @Override
    public void bids(int own, int other) {
        if (own == other) {
            myQuantity++;
            enemyQuantity++;
            lastGameWin = true;
        } else if (own > other) {
            myQuantity += 2;
            lastGameWin = true;
        } else {
            enemyQuantity += 2;
            lastGameWin = false;
        }

        quantity -= 2;
        myCash -= own;
        enemyCash -= other;
        enemyLastBids.add(other);
        myLastBid = own;
        if (numberNonEmptyLastBids < BidderConst.LAST_ENEMY_STEPS_NUMBER) {
            numberNonEmptyLastBids++;
        }
    }

    protected double handleGeneralParametersStep() {
        double quantityDifCoefficient;

        // we need to set medium bid at the beginning of a game
        if (myQuantity == 0 && enemyQuantity == 0) {
            quantityDifCoefficient = BidderCoefficient.MEDIUM_DIF_QUANTITY_COEFFICIENT;
        } else {
            quantityDifCoefficient = calculateQuantityDifCoefficient();
        }

        return (myCash / quantity) * quantityDifCoefficient;
    }

    protected double predictEnemyBidStep() {
        if (numberNonEmptyLastBids > 0) {
            if (lastGameWin) {
                return myLastBid * BidderCoefficient.ENEMY_INCREASE_LOST_BID; // suppose that enemy increase previous
            } else {
                double lastEnemyBidsSum = (double) enemyLastBids.stream()
                        .reduce(0, Integer::sum);
                double averageEnemyBid = lastEnemyBidsSum / numberNonEmptyLastBids;
                double predictedEnemyBid = ((enemyCash / (quantity / 2)) + averageEnemyBid) / BidderCoefficient.AVERAGE_PREDICTED_ENEMY_BID_COEFFICIENT;
                return Math.min(predictedEnemyBid, enemyCash); // enemy can't use more than current cash
            }
        }
        return (enemyCash / (quantity / 2));
    }

    protected double handleBidAccordingEnemyPredictedBidStep(double myBid, double predictedEnemyBid) {
        double handledBid = myBid;
        double currentQuantityDif = myQuantity - enemyQuantity;
        double currentBidDif = myBid - predictedEnemyBid;
        log.info("CurrentQuantityDif = " + currentQuantityDif
                + ", CurrentBidDif = " + currentBidDif);
        if (currentQuantityDif <= -quantity * BidderCoefficient.EQUALITY_QUANTITY_COEFFICIENT) {
            if (currentBidDif <= 0) {
                log.info("The bidder is losing the game and will lose current bid. The bidder increases current bid");
                handledBid = increaseBidAccordingEnemyPredictedBid(myBid, predictedEnemyBid);
            }
        } else if (currentQuantityDif > -quantity * BidderCoefficient.EQUALITY_QUANTITY_COEFFICIENT
                && currentQuantityDif < quantity * BidderCoefficient.EQUALITY_QUANTITY_COEFFICIENT) {
            if (currentBidDif < 0) {
                log.info("The bidder is equal but will lose current bid. The bidder decreases the bid and keep money");
                handledBid = decreaseBidAccordingEnemyPredictedBid(myBid, predictedEnemyBid);
            }
        } else if (currentQuantityDif >= quantity * BidderCoefficient.EQUALITY_QUANTITY_COEFFICIENT) {
            if (currentBidDif <= 0) {
                log.info("The bidder is winning and will lose current bid. The bidder increases the bid because wants to finish the game");
                handledBid = increaseBidAccordingEnemyPredictedBid(myBid, predictedEnemyBid);
            }
        }

        if (handledBid < 0) {
            handledBid = 1;
        }
        return handledBid;
    }

    protected double useRandomStep(double myBid) {
        if (myBid > 0) {
            final NormalDistribution normalDistribution = new NormalDistribution(myBid, myBid * BidderCoefficient.RANDOM_COEFFICIENT);
            myBid = normalDistribution.sample();
        }
        return myBid;
    }

    protected int checkBidStep(double myBid) {
        if (myBid < 0) {
            myBid = 0.0;
        } else if (myBid > myCash || quantity <= 2) {
            myBid = myCash;
        }
        return Math.toIntExact(Math.round(myBid));
    }

    protected double calculateQuantityDifCoefficient() {
        double currentQuantityDif = myQuantity - enemyQuantity;
        if (currentQuantityDif < quantity / 2 || currentQuantityDif > quantity / 2) {
            return BidderCoefficient.HIGH_DIF_QUANTITY_COEFFICIENT;
        }
        final double[] yPoints = {BidderCoefficient.HIGH_DIF_QUANTITY_COEFFICIENT,
                BidderCoefficient.LOW_DIF_QUANTITY_COEFFICIENT,
                BidderCoefficient.MEDIUM_DIF_QUANTITY_COEFFICIENT,
                BidderCoefficient.LOW_DIF_QUANTITY_COEFFICIENT,
                BidderCoefficient.HIGH_DIF_QUANTITY_COEFFICIENT};
        final double[] xPoints = {-quantity / 2,
                -quantity / 4,
                0.0,
                quantity / 4,
                quantity / 2};
        final AkimaSplineInterpolator interpolator = new AkimaSplineInterpolator();
        final PolynomialSplineFunction function = interpolator.interpolate(xPoints, yPoints);

        double quantityDifCoefficient = function.value(currentQuantityDif);
        log.info("quantityDifCoefficient = " + quantityDifCoefficient);
        return quantityDifCoefficient;
    }

    protected double increaseBidAccordingEnemyPredictedBid(double myBid, double predictedEnemyBid) {
        return myBid + BidderCoefficient.BID_ACCORDING_PREDICTED_ENEMY_BID_COEFFICIENT * (predictedEnemyBid - myBid);
    }

    protected double decreaseBidAccordingEnemyPredictedBid(double myBid, double predictedEnemyBid) {
        return myBid - BidderCoefficient.BID_ACCORDING_PREDICTED_ENEMY_BID_COEFFICIENT * (predictedEnemyBid - myBid);
    }

}

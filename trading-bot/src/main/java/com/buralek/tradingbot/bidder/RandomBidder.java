package com.buralek.tradingbot.bidder;

/*
 This bidder places a random bid regarding the randomCoefficient. The random coefficient equals startedCash / 4
 */
public class RandomBidder implements Bidder {
    protected double quantity;
    protected double cash;
    protected double randomCoefficient;

    @Override
    public void init(int quantity, int cash) {
        this.quantity = quantity;
        this.cash = cash;
        this.randomCoefficient = this.cash / 4;
    }

    @Override
    public int placeBid() {
        double randomBid = Math.random() * randomCoefficient;
        if (randomBid > cash) {
            randomBid = cash;
        }
        return Math.toIntExact(Math.round(randomBid));
    }

    @Override
    public void bids(int own, int other) {
        quantity -= 2;
        cash -= own;
    }
}

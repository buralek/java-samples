package com.buralek.tradingbot.bidder;

/*
 This bidder places the same equal bid regarding the formula cash / (quantity * 0.5))
 */
public class EvenlyBidder implements Bidder {
    protected double quantity;
    protected double cash;

    @Override
    public void init(int quantity, int cash) {
        this.quantity = quantity;
        this.cash = cash;
    }

    @Override
    public int placeBid() {
        return Math.toIntExact(Math.round(cash / (quantity * 0.5)));
    }

    @Override
    public void bids(int own, int other) {
        quantity -= 2;
        cash -= own;
    }
}

package com.buralek.tradingbot;

import com.buralek.tradingbot.bidder.Bidder;
import com.buralek.tradingbot.bidder.BuralekBidder;
import com.buralek.tradingbot.constant.AuctionUtilConst;
import com.buralek.tradingbot.util.AuctionUtil;
import com.buralek.tradingbot.util.AuctionUtilImpl;

import java.util.Scanner;

public class ManualGame {
    public static final int QUANTITY = 20;
    public static final int CASH_START = 100;

    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);

        //init BuralekBidder
        final Bidder buralekBidder = new BuralekBidder();
        buralekBidder.init(QUANTITY, CASH_START);

        //init Auction
        final AuctionUtil auctionUtil = new AuctionUtilImpl(QUANTITY, CASH_START);

        //game
        int buralekBid = 0;
        int manualBid = 0;
        String winner;
        while (auctionUtil.getQuantity() > 0) {
            System.out.println("Remained quantity = " + auctionUtil.getQuantity());
            System.out.println("Your cash = " + auctionUtil.getBidder1Cash()
                    + "\nYour quantity = " + auctionUtil.getBidder1Quantity());
            System.out.println("Enemy cash = " + auctionUtil.getBidder2Cash()
                    + "\nEnemy quantity = " + auctionUtil.getBidder2Quantity());

            manualBid = getCurrentManualBid(inputScanner, auctionUtil);
            buralekBid = buralekBidder.placeBid();

            buralekBidder.bids(buralekBid, manualBid);
            winner = auctionUtil.finishCurrentAuction(manualBid, buralekBid);
            System.out.println("Your bid is = " + manualBid);
            System.out.println("Enemy bid is = " + buralekBid);
            System.out.println("Winner is = " + winner);
            System.out.println("---------------------------------------------------");
        }
        String result = auctionUtil.result();
        switch (result) {
            case AuctionUtilConst.BIDDER_1_NAME -> System.out.println("Congratulations! You are the winner!");
            case AuctionUtilConst.BIDDER_2_NAME -> System.out.println("Sorry, you lost the game!");
            case AuctionUtilConst.EQUALITY_NAME -> System.out.println("Game has no winner");
        }
    }

    private static int getCurrentManualBid(final Scanner inputScanner, AuctionUtil auctionUtil) {

        int manualBid = -1;
        while (manualBid < 0 || manualBid > auctionUtil.getBidder1Cash()) {
            System.out.println("Enter your bid");
            if (inputScanner.hasNextInt()) {
                manualBid = inputScanner.nextInt();
                if (manualBid < 0 || manualBid > auctionUtil.getBidder1Cash()) {
                    System.out.println("Illegal bid. Please try again.");
                }
            } else {
                inputScanner.nextLine();
                System.out.println("Illegal input. Please, enter numbers only");
            }

        }
        return manualBid;
    }
}

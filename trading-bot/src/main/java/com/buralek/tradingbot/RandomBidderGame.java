package com.buralek.tradingbot;

import com.buralek.tradingbot.bidder.Bidder;
import com.buralek.tradingbot.bidder.BuralekBidder;
import com.buralek.tradingbot.bidder.RandomBidder;
import com.buralek.tradingbot.constant.AuctionUtilConst;
import com.buralek.tradingbot.util.AuctionUtil;
import com.buralek.tradingbot.util.AuctionUtilImpl;

public class RandomBidderGame {
    public static final int QUANTITY = 20;
    public static final int CASH_START = 100;

    public static void main(String[] args) {
        //init BuralekBidder
        final Bidder buralekBidder = new BuralekBidder();
        buralekBidder.init(QUANTITY, CASH_START);

        //init RandomBidder
        final Bidder randomBidder = new RandomBidder();
        randomBidder.init(QUANTITY, CASH_START);

        //init Auction
        final AuctionUtil auctionUtil = new AuctionUtilImpl(QUANTITY, CASH_START);

        //game
        int buralekBid = 0;
        int randomBid = 0;
        String winner;
        while (auctionUtil.getQuantity() > 0) {
            System.out.println("Remained quantity = " + auctionUtil.getQuantity());
            System.out.println("Random bidder(bidder1)  cash = " + auctionUtil.getBidder1Cash()
                    + "\nRandom bidder(bidder1)  quantity = " + auctionUtil.getBidder1Quantity());
            System.out.println("Buralek bidder(bidder2) cash = " + auctionUtil.getBidder2Cash()
                    + "\nBuralek bidder(bidder2) quantity = " + auctionUtil.getBidder2Quantity());

            randomBid = randomBidder.placeBid();
            buralekBid = buralekBidder.placeBid();

            buralekBidder.bids(buralekBid, randomBid);
            randomBidder.bids(randomBid, buralekBid);
            winner = auctionUtil.finishCurrentAuction(randomBid, buralekBid);
            System.out.println("Random bidder(bidder1) bid is = " + randomBid);
            System.out.println("Buralek bidder(bidder2) bid is = " + buralekBid);
            System.out.println("Winner is = " + winner);
            System.out.println("---------------------------------------------------");
        }
        String result = auctionUtil.result();
        switch (result) {
            case AuctionUtilConst.BIDDER_1_NAME -> System.out.println("Random bidder(bidder1) is the winner!");
            case AuctionUtilConst.BIDDER_2_NAME -> System.out.println("Buralek bidder(bidder2) is the winner!");
            case AuctionUtilConst.EQUALITY_NAME -> System.out.println("Game has no winner");
        }
    }
}

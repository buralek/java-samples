package com.buralek.tradingbot.bidder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BuralekBidderTest {
    private final int INIT_QUANTITY = 20;
    private final int INIT_CASH = 100;

    private BuralekBidder buralekBidder;

    @BeforeEach
    public void setup() {
        buralekBidder = new BuralekBidder();
        buralekBidder.init(INIT_QUANTITY, INIT_CASH);
    }

    @Test
    public void testInit() {
        final int TEST_QUANTITY = 30;
        final int TEST_CASH = 200;
        buralekBidder.init(TEST_QUANTITY, TEST_CASH);

        assertEquals(buralekBidder.quantity, TEST_QUANTITY);

        assertEquals(buralekBidder.myCash, TEST_CASH);
        assertEquals(buralekBidder.myQuantity, 0);
        assertEquals(buralekBidder.myLastBid, 0);
        assertFalse(buralekBidder.lastGameWin);

        assertEquals(buralekBidder.enemyCash, TEST_CASH);
        assertEquals(buralekBidder.enemyQuantity, 0);
        assertNotEquals(buralekBidder.enemyLastBids, null);
        assertEquals(buralekBidder.numberNonEmptyLastBids, 0);
    }

    @Test
    public void testPlaceBid() {
        int bid = buralekBidder.placeBid();
        assertTrue(bid >= 0);
    }

    @Test
    public void testBidsEqual() {
        final int OWN_BID = 20;
        final int OTHER_BID = 20;

        final double MY_QUANTITY = buralekBidder.myQuantity + 1;
        final double ENEMY_QUANTITY = buralekBidder.enemyQuantity + 1;
        final boolean LAST_GAME_WIN = true;
        final double QUANTITY = buralekBidder.quantity - 2;
        final double MY_CASH = buralekBidder.myCash - OWN_BID;
        final double ENEMY_CASH = buralekBidder.enemyCash - OTHER_BID;
        final int NUMBER_NON_EMPTY_LAST_BIDS = buralekBidder.numberNonEmptyLastBids + 1;

        buralekBidder.bids(OWN_BID, OTHER_BID);

        assertEquals(buralekBidder.myQuantity, MY_QUANTITY);
        assertEquals(buralekBidder.enemyQuantity, ENEMY_QUANTITY);
        assertEquals(buralekBidder.lastGameWin, LAST_GAME_WIN);
        assertEquals(buralekBidder.quantity, QUANTITY);
        assertEquals(buralekBidder.myCash, MY_CASH);
        assertEquals(buralekBidder.enemyCash, ENEMY_CASH);
        assertEquals(buralekBidder.numberNonEmptyLastBids, NUMBER_NON_EMPTY_LAST_BIDS);
        assertEquals(buralekBidder.enemyLastBids.size(), 1);
        assertEquals(buralekBidder.myLastBid, OWN_BID);
    }

    @Test
    public void testBidsBidderWin() {
        final int OWN_BID = 20;
        final int OTHER_BID = 10;

        final double MY_QUANTITY = buralekBidder.myQuantity + 2;
        final double ENEMY_QUANTITY = buralekBidder.enemyQuantity;
        final boolean LAST_GAME_WIN = true;
        final double QUANTITY = buralekBidder.quantity - 2;
        final double MY_CASH = buralekBidder.myCash - OWN_BID;
        final double ENEMY_CASH = buralekBidder.enemyCash - OTHER_BID;
        final int NUMBER_NON_EMPTY_LAST_BIDS = buralekBidder.numberNonEmptyLastBids + 1;

        buralekBidder.bids(OWN_BID, OTHER_BID);

        assertEquals(buralekBidder.myQuantity, MY_QUANTITY);
        assertEquals(buralekBidder.enemyQuantity, ENEMY_QUANTITY);
        assertEquals(buralekBidder.lastGameWin, LAST_GAME_WIN);
        assertEquals(buralekBidder.quantity, QUANTITY);
        assertEquals(buralekBidder.myCash, MY_CASH);
        assertEquals(buralekBidder.enemyCash, ENEMY_CASH);
        assertEquals(buralekBidder.numberNonEmptyLastBids, NUMBER_NON_EMPTY_LAST_BIDS);
        assertEquals(buralekBidder.enemyLastBids.size(), 1);
        assertEquals(buralekBidder.myLastBid, OWN_BID);
    }

    @Test
    public void testBidsEnemyWin() {
        final int OWN_BID = 10;
        final int OTHER_BID = 20;

        final double MY_QUANTITY = buralekBidder.myQuantity;
        final double ENEMY_QUANTITY = buralekBidder.enemyQuantity + 2;
        final boolean LAST_GAME_WIN = false;
        final double QUANTITY = buralekBidder.quantity - 2;
        final double MY_CASH = buralekBidder.myCash - OWN_BID;
        final double ENEMY_CASH = buralekBidder.enemyCash - OTHER_BID;
        final int NUMBER_NON_EMPTY_LAST_BIDS = buralekBidder.numberNonEmptyLastBids + 1;

        buralekBidder.bids(OWN_BID, OTHER_BID);

        assertEquals(buralekBidder.myQuantity, MY_QUANTITY);
        assertEquals(buralekBidder.enemyQuantity, ENEMY_QUANTITY);
        assertEquals(buralekBidder.lastGameWin, LAST_GAME_WIN);
        assertEquals(buralekBidder.quantity, QUANTITY);
        assertEquals(buralekBidder.myCash, MY_CASH);
        assertEquals(buralekBidder.enemyCash, ENEMY_CASH);
        assertEquals(buralekBidder.numberNonEmptyLastBids, NUMBER_NON_EMPTY_LAST_BIDS);
        assertEquals(buralekBidder.enemyLastBids.size(), 1);
        assertEquals(buralekBidder.myLastBid, OWN_BID);
    }

    // TODO need to add tests for all steps(handleGeneralParametersStep, predictEnemyBidStep, handleBidAccordingEnemyPredictedBidStep, useRandomStep and checkBidStep).
    // Sorry, I didn't have free time for this.
    // Hope that current tests are enough to show that I know how to write tests :)
}

package com.buralek.tradingbot.bidder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EvenlyBidderTest {
    private final int INIT_QUANTITY = 20;
    private final int INIT_CASH = 100;

    private EvenlyBidder evenlyBidder;

    @BeforeEach
    public void setup() {
        evenlyBidder = new EvenlyBidder();
        evenlyBidder.init(INIT_QUANTITY, INIT_CASH);
    }

    @Test
    public void testInit() {
        final int TEST_QUANTITY = 30;
        final int TEST_CASH = 200;
        evenlyBidder.init(TEST_QUANTITY, TEST_CASH);
        assertEquals(evenlyBidder.quantity, TEST_QUANTITY);
        assertEquals(evenlyBidder.cash, TEST_CASH);
    }

    @Test
    public void testPlaceBid() {
        final int CHECK_BID = Math.toIntExact(Math.round(INIT_CASH / (INIT_QUANTITY * 0.5)));
        int currentBid = evenlyBidder.placeBid();
        assertEquals(currentBid, CHECK_BID);
    }

    @Test
    public void testBids() {
        final int OWN_BID = 10;
        final int OTHER_BID = 20;
        evenlyBidder.bids(OWN_BID, OTHER_BID);
        assertEquals(evenlyBidder.quantity, INIT_QUANTITY - 2);
        assertEquals(evenlyBidder.cash, INIT_CASH - OWN_BID);
    }
}

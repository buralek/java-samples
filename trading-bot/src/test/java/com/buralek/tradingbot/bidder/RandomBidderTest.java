package com.buralek.tradingbot.bidder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RandomBidderTest {
    private final int INIT_QUANTITY = 20;
    private final int INIT_CASH = 100;

    private RandomBidder randomBidder;

    @BeforeEach
    public void setup() {
        randomBidder = new RandomBidder();
        randomBidder.init(INIT_QUANTITY, INIT_CASH);
    }

    @Test
    public void testInit() {
        final int TEST_QUANTITY = 30;
        final int TEST_CASH = 200;
        randomBidder.init(TEST_QUANTITY, TEST_CASH);
        assertEquals(randomBidder.quantity, TEST_QUANTITY);
        assertEquals(randomBidder.cash, TEST_CASH);
    }

    @Test
    public void testPlaceBid() {
        int bid = randomBidder.placeBid();
        assertTrue(bid >= 0);
    }

    @Test
    public void testBids() {
        final int OWN_BID = 10;
        final int OTHER_BID = 20;
        randomBidder.bids(OWN_BID, OTHER_BID);
        assertEquals(randomBidder.quantity, INIT_QUANTITY - 2);
        assertEquals(randomBidder.cash, INIT_CASH - OWN_BID);
    }
}

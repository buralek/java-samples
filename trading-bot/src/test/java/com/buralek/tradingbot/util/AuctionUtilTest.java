package com.buralek.tradingbot.util;

import com.buralek.tradingbot.constant.AuctionUtilConst;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuctionUtilTest {
    private final int INIT_QUANTITY = 20;
    private final int INIT_CASH = 100;

    private AuctionUtil auctionUtil;

    @BeforeEach
    public void setup() {
        auctionUtil = new AuctionUtilImpl(INIT_QUANTITY, INIT_CASH);
    }

    @Test
    public void testFinishCurrentAuctionEqual() {
        final int BID_1 = 10;
        final int BID_2 = 10 ;

        final double QUANTITY = auctionUtil.getQuantity() - 2;
        final double BIDDER_1_QUANTITY = auctionUtil.getBidder1Quantity() + 1;
        final double BIDDER_1_CASH = auctionUtil.getBidder1Cash() - BID_1;
        final double BIDDER_2_QUANTITY = auctionUtil.getBidder2Quantity() + 1;
        final double BIDDER_2_CASH = auctionUtil.getBidder2Cash() - BID_2;

        String winner = auctionUtil.finishCurrentAuction(BID_1, BID_2);

        assertEquals(winner, AuctionUtilConst.EQUALITY_NAME);
        assertEquals(auctionUtil.getQuantity(), QUANTITY);
        assertEquals(auctionUtil.getBidder1Quantity(), BIDDER_1_QUANTITY);
        assertEquals(auctionUtil.getBidder1Cash(), BIDDER_1_CASH);
        assertEquals(auctionUtil.getBidder2Quantity(), BIDDER_2_QUANTITY);
        assertEquals(auctionUtil.getBidder2Cash(), BIDDER_2_CASH);
    }

    @Test
    public void testFinishCurrentAuctionBidder1Win() {
        final int BID_1 = 20;
        final int BID_2 = 10 ;

        final double QUANTITY = auctionUtil.getQuantity() - 2;
        final double BIDDER_1_QUANTITY = auctionUtil.getBidder1Quantity() + 2;
        final double BIDDER_1_CASH = auctionUtil.getBidder1Cash() - BID_1;
        final double BIDDER_2_QUANTITY = auctionUtil.getBidder2Quantity();
        final double BIDDER_2_CASH = auctionUtil.getBidder2Cash() - BID_2;

        String winner = auctionUtil.finishCurrentAuction(BID_1, BID_2);

        assertEquals(winner, AuctionUtilConst.BIDDER_1_NAME);
        assertEquals(auctionUtil.getQuantity(), QUANTITY);
        assertEquals(auctionUtil.getBidder1Quantity(), BIDDER_1_QUANTITY);
        assertEquals(auctionUtil.getBidder1Cash(), BIDDER_1_CASH);
        assertEquals(auctionUtil.getBidder2Quantity(), BIDDER_2_QUANTITY);
        assertEquals(auctionUtil.getBidder2Cash(), BIDDER_2_CASH);
    }

    @Test
    public void testFinishCurrentAuctionBidder2Win() {
        final int BID_1 = 10;
        final int BID_2 = 20 ;

        final double QUANTITY = auctionUtil.getQuantity() - 2;
        final double BIDDER_1_QUANTITY = auctionUtil.getBidder1Quantity();
        final double BIDDER_1_CASH = auctionUtil.getBidder1Cash() - BID_1;
        final double BIDDER_2_QUANTITY = auctionUtil.getBidder2Quantity() + 2;
        final double BIDDER_2_CASH = auctionUtil.getBidder2Cash() - BID_2;

        String winner = auctionUtil.finishCurrentAuction(BID_1, BID_2);

        assertEquals(winner, AuctionUtilConst.BIDDER_2_NAME);
        assertEquals(auctionUtil.getQuantity(), QUANTITY);
        assertEquals(auctionUtil.getBidder1Quantity(), BIDDER_1_QUANTITY);
        assertEquals(auctionUtil.getBidder1Cash(), BIDDER_1_CASH);
        assertEquals(auctionUtil.getBidder2Quantity(), BIDDER_2_QUANTITY);
        assertEquals(auctionUtil.getBidder2Cash(), BIDDER_2_CASH);
    }

    @Test
    public void testResultEqual() {
        auctionUtil.setBidder1Quantity(10);
        auctionUtil.setBidder2Quantity(10);
        String winner = auctionUtil.result();
        assertEquals(winner, AuctionUtilConst.EQUALITY_NAME);
    }

    @Test
    public void testResultBidder1Win() {
        auctionUtil.setBidder1Quantity(20);
        auctionUtil.setBidder2Quantity(10);
        String winner = auctionUtil.result();
        assertEquals(winner, AuctionUtilConst.BIDDER_1_NAME);
    }

    @Test
    public void testResultBidder2Win() {
        auctionUtil.setBidder1Quantity(10);
        auctionUtil.setBidder2Quantity(20);
        String winner = auctionUtil.result();
        assertEquals(winner, AuctionUtilConst.BIDDER_2_NAME);
    }
}

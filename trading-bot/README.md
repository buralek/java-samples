# Trading bot
**This application realizes the next task:**

A product x QU (quantity units) will be auctioned under 2 parties. The parties have each y MU (monetary units) for auction. They offer then simultaneously an arbitrary number of its MU on the first 2 QU of the product. After that, the bids will be visible to both. The 2 QU of the product is awarded to who has offered the most MU; if both bid the same, then both get 1 QU. Both bidders must pay their amount - including the defeated. A bid of 0 MU is allowed. Bidding on each 2 QU is repeated until the supply of x QU is fully auctioned. Each bidder aims to get a larger amount than its competitor.

In an auction wins the program that is able to get more QU than the other. With a tie, the program that retains more MU wins. Write a program that can participate in such an auction and competes with one of our programs. Please explain its strategy.


**How to use**

1) Use all three bidders in your code if you add trading-bot.jar into your dependencies.
2) Start ManualGame and play manually against BuralekBidder.
   java -Djava.util.logging.config.file=logging.properties -classpath trading-bot-1.0-SNAPSHOT-jar-with-dependencies.jar com.buralek.tradingbot.ManualGame
3) Start EvenlyBidderGame and show EvenlyBidder vs BuralekBidder competition.
   java -Djava.util.logging.config.file=logging.properties -classpath trading-bot-1.0-SNAPSHOT-jar-with-dependencies.jar com.buralek.tradingbot.EvenlyBidderGame
3) Start RandomBidderGame and show RandomBidderGame vs BuralekBidder competition.
   java -Djava.util.logging.config.file=logging.properties -classpath trading-bot-1.0-SNAPSHOT-jar-with-dependencies.jar com.buralek.tradingbot.RandomBidderGame

**BuralekBidder**

This is a try to realize a very simple algorithm for the current task.
We need to solve repeated game depended on history.

The algorithm contains 5 steps. You can extend BuralekBidder, override each of them or just change coefficients in BidderCoefficient:
1) **HandleGeneralParametersStep**. We calculate a very simple formula (myCash / quantity) * quantityDifCoefficient. 
   But the trick is in quantityDifCoefficient. 
   ![img.png](quantityDifCoefficient.png)
2) **PredictEnemyBidStep**. This is the hardest step. It's very hard to predict the next enemy step. My simple solution is
   (enemyCash / (quantity / 2)) + averageEnemyBid) / BidderCoefficient.AVERAGE_PREDICTED_ENEMY_BID_COEFFICIENT
3) **handleBidAccordingEnemyPredictedBidStep**. In this step we have to shift myBid according predictedEnemyBid
![img.png](handleBidAccordingEnemyPredictedTable.png)
   
4) **useRandomStep**. Random is a good idea every time :) However, we do normal gaussian random around current bid
5) **checkBidStep**. Need to check that current bid > 0 and current bid <= currentCash

**EvenlyBidderGame**

This bidder places the same equal bid regarding the formula cash / (quantity * 0.5)).

**RandomBidderGame**

This bidder places a random bid regarding the randomCoefficient. The random coefficient equals startedCash / 4.



package com.buralek.knapsack;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    private static int resultWeight;
    private static int itemNumber;
    private static Integer[] values;
    private static Integer[] weights;

    public static void main(String[] args) throws IOException{
        prepareData(args[0]);
        simpleSolution(values, weights);
        System.out.println();
        hashSolution(values, weights);
    }

    private static void simpleSolution(Integer[] values, Integer[] weights) {
        int[][] result = new int[itemNumber + 1][resultWeight + 1];
        int counter = 0;

        System.out.println("Simple solution");
        Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("Start " + startTimestamp);
        for (int n = 1; n <= itemNumber; n++) {
            for (int w = 1; w <= resultWeight; w++) {
                counter++;
                if (weights[n] > w) {
                    result[n][w] = result[n - 1][w];
                }
                else {
                    result[n][w] = Math.max(result[n - 1][w], result[n - 1][w - weights[n]] + values[n]);
                }
            }
        }
        Timestamp endTimestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("End " + endTimestamp);
        System.out.println("Dif " + (endTimestamp.getTime() - startTimestamp.getTime()));

        System.out.println("Counter =  " + counter);
        //printArrayResult(result);
        System.out.println("Solution is " + result[itemNumber][resultWeight]);
    }

    private static void hashSolution(Integer[] values, Integer[] weights) {
        Map<Integer, Integer> result = new HashMap<>();
        result.put(0,0);
        int counter = 0;

        System.out.println("Hash solution");
        Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("Start " + startTimestamp);
        for (int n = 1; n <= itemNumber; n++) {
            Map<Integer, Integer> newResult = new HashMap<>();
            newResult.put(0,0);
            for (Integer key : result.keySet()) {
                int tempWeight = weights[n] + key;
                if (tempWeight <= resultWeight) {
                    int tmpValue = values[n] + result.get(key);
                    counter++;
                    if (result.containsKey(tempWeight)) {
                        newResult.put(tempWeight, Math.max(tmpValue, result.get(tempWeight)));
                    } else {
                        newResult.put(tempWeight, tmpValue);
                    }
                }
            }
            copyMap(result, newResult);
        }
        Timestamp endTimestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("End " + endTimestamp);
        System.out.println("Dif " + (endTimestamp.getTime() - startTimestamp.getTime()));

        System.out.println("Counter =  " + counter);
        //System.out.println("Result map: " + result);
        System.out.println("Solution is " + result.get(resultWeight));
    }

    private static void printArrayResult(int[][] result) {
        System.out.println("Result table:");
        for (int n = 0; n <= itemNumber; n++) {
            for (int w = 0; w <= resultWeight; w++) {
                System.out.print(result[n][w] + ",");
            }
            System.out.println();
        }
    }

    private static void copyMap(Map<Integer, Integer> oldMap, Map<Integer, Integer> newMap) {
        for (Integer key : newMap.keySet()) {
            oldMap.put(key, newMap.get(key));
        }
    }

    private static void prepareData(String filePath) throws IOException {
        List<Integer> valueList = new ArrayList<>();
        valueList.add(0);
        List<Integer> weightList = new ArrayList<>();
        weightList.add(0);
        boolean firstLine = true;

        Path path = Paths.get(filePath);
        for (String line : Files.readAllLines(path)) {
            String[] splitLine = line.split(" ");
            if (firstLine) {
                itemNumber = Integer.parseInt(splitLine[0]);
                resultWeight = Integer.parseInt(splitLine[1]);
                firstLine = false;
            } else {
                valueList.add(Integer.valueOf(splitLine[0]));
                weightList.add(Integer.valueOf(splitLine[1]));
            }
        }

        values = valueList.toArray(new Integer[0]);
        weights = weightList.toArray(new Integer[0]);
    }
}

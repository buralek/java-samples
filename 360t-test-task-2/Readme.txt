# 360T Task

## Task description

Having a Player class - an instance of this class with that can communicate with other Player(s) (other instances of this class)

The use case for this task is as bellow:

1. create 2 players

2. one of the players should send a message to second player (let's call this player "initiator")

3. when a player receives a message should send back a new message that contains the received message concatenated with the message counter that this player sent.

4. finalize the program (gracefully) after the initiator sent 10 messages and received back 10 messages (stop condition)

5. both players should run in the same java process (strong requirement)

6. document for every class the responsibilities it has.

7. opposite to 5: have every player in a separate JAVA process (different PID).

Please use pure Java as much as possible (no additional frameworks like spring, etc.)
Please deliver one single maven project with the source code only (no jars). Please send the maven project as archive attached to e-mail (eventual links for download will be ignored due to security policy).
Please provide a shell script to start the program.
Everything what is not clearly specified is to be decided by developer. Everything what is specified is a hard requirement.
Please focus on design and not on technology, the technology should be the simplest possible that is achieving the target.
The focus of the exercise is to deliver the cleanest and clearest design that you can achieve (and the system has to be functional).

## Installation

```bash
./start.sh
```

## Solutions
The application contains a server and 2 clients.
1. Server starts and waits 2 clients.
2. The initiator client starts, connects to the server and sends the first message.
3. The second client starts and connects to the server.
4. The server checks that there are CLIENTS_NUMBER connections, handles the first message and starts the game
5. When the initiator player has sent and received MAX_MESSAGES, the initiator sends a stop event and stops.
6. The server handles the stop event and notifies the second client.
7. The second client receives the stop event and stops.
8. Server stops because all clients have been stopped.

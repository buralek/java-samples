package com.buralek.t360.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.List;

import static com.buralek.t360.shared.Constant.*;

/**
 * This class creates clientHandlers for each client
 * It stops if here are no alive clientHandlers and by serverSocket timeout
 */
public class Server implements Runnable {
    private List<ClientHandler> clientHandlers;

    public Server() {
        clientHandlers = new LinkedList<>();
    }

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
            serverSocket.setSoTimeout(SERVER_ACCEPT_TIMEOUT);
            Socket socket;
            while (isAliveHandlers()) {
                socket = serverSocket.accept();
                ClientHandler currentClientHandler = new ClientHandler(socket, clientHandlers);
                clientHandlers.add(currentClientHandler);
                Thread clientHandlerThread = new Thread(currentClientHandler);
                clientHandlerThread.start();
            }
        } catch (SocketTimeoutException e) {
            System.out.println("Server stopped. Please click Enter button to exit");
        } catch (IOException e) {
            System.out.println("Server exception: " + e);
        }
    }

    /**
     * Check alive handlers
     *
     * @return true if here is alive handler
     */
    protected boolean isAliveHandlers() {
        long numberOfClosedClientHandlers = clientHandlers.stream().filter(ClientHandler::isClosed).count();
        return numberOfClosedClientHandlers != CLIENTS_NUMBER;
    }
}

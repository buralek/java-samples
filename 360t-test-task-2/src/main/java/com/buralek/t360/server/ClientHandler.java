package com.buralek.t360.server;

import com.buralek.t360.shared.event.EventTypes;
import com.buralek.t360.shared.event.GameEvent;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.stream.Collectors;

import static com.buralek.t360.shared.Constant.CLIENTS_NUMBER;
import static com.buralek.t360.shared.Constant.THREAD_DELAY;

/**
 * Create an instance of the class for each client
 */
public class ClientHandler implements Runnable {
    private Socket socket;
    private String eventType;
    private boolean isClosed;
    private List<ClientHandler> clientHandlers;

    public ClientHandler(Socket socket, List<ClientHandler> clientHandlers) {
        this.socket = socket;
        this.clientHandlers = clientHandlers;
        isClosed = false;
    }

    public void run() {
        try {
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

            // each client sends EventType to server
            while (eventType == null) {
                GameEvent gameEvent = (GameEvent) inputStream.readObject();
                System.out.println("Server received an event: " + gameEvent);
                updateEventType(gameEvent);
            }

            // wait that all clients are ready to play
            while (!allClientsAreReady()) {
                Thread.sleep(1000);
            }

            // handle events
            while (true) {
                GameEvent gameEvent = (GameEvent) inputStream.readObject();
                for (ClientHandler clientHandler : clientHandlers) {
                    if (needToHandle(gameEvent, clientHandler)) {
                        sendEvent(gameEvent, clientHandler);
                    }
                }
                Thread.sleep(THREAD_DELAY);

            }
        } catch (EOFException e) {

        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            System.out.println("Server exception: " + e);
        } finally {
            //close socket
            try {
                if (!socket.isClosed()) {
                    socket.close();
                }
                isClosed = true;
            } catch (IOException e) {
                System.out.println("Server exception: " + e);
            }
        }
    }

    /**
     * Send an event to the client
     *
     * @param gameEvent     - a received event
     * @param clientHandler - clientHandler which will send the event
     * @throws IOException
     */
    protected void sendEvent(GameEvent gameEvent, ClientHandler clientHandler) throws IOException {
        Socket socketToSend = clientHandler.getSocket();
        ObjectOutputStream outputStream = new ObjectOutputStream(socketToSend.getOutputStream());
        outputStream.writeObject(gameEvent);
        outputStream.flush();
        System.out.println("Server received and sent an event: " + gameEvent);
    }

    /**
     * Update eventType
     *
     * @param gameEvent - MY_EVENT event for updating
     */
    protected void updateEventType(GameEvent gameEvent) {
        if (gameEvent.getEventType().equals(EventTypes.MY_EVENT)) {
            eventType = gameEvent.getMessage();
        }
    }

    /**
     * Check which clientHandler must handle the event
     *
     * @param gameEvent
     * @param clientHandler
     * @return true if this clientHandler must handle the event
     */
    protected boolean needToHandle(GameEvent gameEvent, ClientHandler clientHandler) {
        String gameEventType = gameEvent.getEventType();
        return gameEventType.equals(EventTypes.STOP_EVENT) || gameEventType.equals(clientHandler.getEventType());
    }

    /**
     * Check that all clients are ready for the game
     *
     * @return true if all clients are ready
     */
    protected boolean allClientsAreReady() {
        if (clientHandlers.size() == CLIENTS_NUMBER) {
            List<ClientHandler> notReadyClients = clientHandlers.stream().filter(clientHandler -> clientHandler.getEventType() == null).collect(Collectors.toList());
            return notReadyClients.size() == 0;
        }
        return false;
    }

    public String getEventType() {
        return eventType;
    }

    public Socket getSocket() {
        return socket;
    }

    public boolean isClosed() {
        return isClosed;
    }
}

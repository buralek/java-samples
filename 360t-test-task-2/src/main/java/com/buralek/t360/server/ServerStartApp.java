package com.buralek.t360.server;

/**
 * This class starts the server
 */
public class ServerStartApp {
    public static void main(String[] args) {
        Server server = new Server();
        Thread serverThread = new Thread(server);
        serverThread.start();
    }
}

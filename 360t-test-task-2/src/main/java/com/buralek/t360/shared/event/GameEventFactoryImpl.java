package com.buralek.t360.shared.event;

public class GameEventFactoryImpl implements GameEventFactory<GameEvent> {
    @Override
    public GameEvent create(String eventType, String message) {
        GameEvent gameEvent = new GameEvent();
        gameEvent.setEventType(eventType);
        gameEvent.setMessage(message);
        return gameEvent;
    }
}

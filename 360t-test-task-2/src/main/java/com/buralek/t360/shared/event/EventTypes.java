package com.buralek.t360.shared.event;

/**
 * List of available game event types
 */
public class EventTypes {
    public final static String PLAYER_1_EVENT = "PLAYER_1_EVENT";
    public final static String PLAYER_2_EVENT = "PLAYER_2_EVENT";
    public final static String STOP_EVENT = "STOP_EVENT";
    public final static String MY_EVENT = "MY_EVENT";
}

package com.buralek.t360.shared;

/**
 * List of constants which are used in the app
 */
public class Constant {
    public static final int SERVER_PORT = 1234;
    public static final String SERVER_ADDRESS = "localhost";
    public static final int MAX_MESSAGES = 10;
    public static final int THREAD_DELAY = 200;
    public static final int CLIENTS_NUMBER = 2;
    public static final int SERVER_ACCEPT_TIMEOUT = 3000;
}

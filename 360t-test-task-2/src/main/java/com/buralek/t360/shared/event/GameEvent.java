package com.buralek.t360.shared.event;

import java.io.Serializable;
import java.util.StringJoiner;

/**
 * Game event which is used in the single thread and multi thread solutions
 */
public class GameEvent implements Serializable {
    private String eventType;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GameEvent.class.getSimpleName() + "[", "]")
                .add("eventType='" + eventType + "'")
                .add("message='" + message + "'")
                .toString();
    }
}

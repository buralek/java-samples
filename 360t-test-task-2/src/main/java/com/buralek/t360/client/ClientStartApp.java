package com.buralek.t360.client;

import com.buralek.t360.shared.event.EventTypes;

/**
 * This class starts the second client
 */
public class ClientStartApp {
    public static void main(String[] args) {
        Client secondClient = new Client("Player2", EventTypes.PLAYER_2_EVENT, EventTypes.PLAYER_1_EVENT);
        Thread secondClientThread = new Thread(secondClient);
        secondClientThread.start();
    }
}

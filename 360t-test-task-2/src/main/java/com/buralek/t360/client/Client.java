package com.buralek.t360.client;

import com.buralek.t360.shared.event.EventTypes;
import com.buralek.t360.shared.event.GameEvent;
import com.buralek.t360.shared.event.GameEventFactory;
import com.buralek.t360.shared.event.GameEventFactoryImpl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import static com.buralek.t360.shared.Constant.*;

/**
 * A client for the second player
 */
public class Client implements Runnable {
    protected String name;
    protected String playerEventType;
    protected String subscribedEventType;
    protected int sentMessagesCounter;
    protected int receivedMessagesCounter;
    protected GameEventFactory gameEventFactory;

    public Client(String name, String playerEventType, String subscribedEventType) {
        this.name = name;
        this.playerEventType = playerEventType;
        this.subscribedEventType = subscribedEventType;
        sentMessagesCounter = 0;
        receivedMessagesCounter = 0;
        gameEventFactory = new GameEventFactoryImpl();
    }

    @Override
    public void run() {
        execute();
    }

    /**
     * Main method which connects to the server and updates messages
     */
    protected void execute() {
        try {
            InetAddress address = InetAddress.getByName(SERVER_ADDRESS);
            Socket socket = new Socket(address, SERVER_PORT);
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());

            sendInitEvent(outputStream);

            while (true) {
                GameEvent gameEvent = receiveEvent(socket);
                if (gameEvent.getEventType().equals(EventTypes.STOP_EVENT)) {
                    break;
                }
                sendEvent(gameEvent, outputStream);
                Thread.sleep(THREAD_DELAY);
            }

            socket.close();

        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            System.out.println("Client exception: " + e);
        }
    }

    /**
     * Send an init event to update clientHandler
     *
     * @param outputStream
     * @throws IOException
     */
    protected void sendInitEvent(ObjectOutputStream outputStream) throws IOException {
        GameEvent gameEvent = gameEventFactory.create(EventTypes.MY_EVENT, subscribedEventType);
        outputStream.writeObject(gameEvent);
        outputStream.flush();
        System.out.println(name + " client sent an init event: " + gameEvent);
    }

    /**
     * Receive an event and update the counter
     *
     * @param socket
     * @return a received event
     * @throws IOException
     * @throws ClassNotFoundException
     */
    protected GameEvent receiveEvent(Socket socket) throws IOException, ClassNotFoundException {
        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
        GameEvent gameEvent = (GameEvent) inputStream.readObject();
        System.out.println(name + " client received a message: " + gameEvent.getMessage());
        receivedMessagesCounter++;
        return gameEvent;
    }

    /**
     * Update message and send event to the server
     *
     * @param gameEvent    - a received event
     * @param outputStream
     * @throws IOException
     */
    protected void sendEvent(GameEvent gameEvent, ObjectOutputStream outputStream) throws IOException {
        String newMessage = updateMessage(gameEvent.getMessage());
        GameEvent newGameEvent = gameEventFactory.create(playerEventType, newMessage);
        System.out.println(name + " client sent a message: " + newGameEvent.getMessage());
        sentMessagesCounter++;
        outputStream.writeObject(newGameEvent);
        outputStream.flush();
    }


    /**
     * Updates the message regarding the rule
     *
     * @param oldMessage message which will be updated
     * @return new updated message
     */
    protected String updateMessage(String oldMessage) {
        return oldMessage + "\\" + sentMessagesCounter;
    }

}

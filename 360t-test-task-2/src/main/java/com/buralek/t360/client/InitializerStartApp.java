package com.buralek.t360.client;

import com.buralek.t360.shared.event.EventTypes;

/**
 * This class starts the initializer client
 */
public class InitializerStartApp {
    public static void main(String[] args) {
        InitializerClient firstClient = new InitializerClient("Player1", EventTypes.PLAYER_1_EVENT, EventTypes.PLAYER_2_EVENT, "FirstMessage");
        Thread firstClientThread = new Thread(firstClient);
        firstClientThread.start();
    }
}

package com.buralek.t360.client;

import com.buralek.t360.shared.event.EventTypes;
import com.buralek.t360.shared.event.GameEvent;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import static com.buralek.t360.shared.Constant.*;

/**
 * InitializerClient is a special client which starts the game
 */
public class InitializerClient extends Client {
    private String firstMessage;

    public InitializerClient(String name, String playerEventType, String subscribedEventType, String firstMessage) {
        super(name, playerEventType, subscribedEventType);
        this.firstMessage = firstMessage;
    }

    /**
     * Execute method which contains a special flow for InitializerClient
     */
    @Override
    protected void execute() {
        try {
            InetAddress address = InetAddress.getByName(SERVER_ADDRESS);
            Socket socket = new Socket(address, SERVER_PORT);
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());

            sendInitEvent(outputStream);

            sendFirstEvent(outputStream);

            while (!endGame()) {
                GameEvent gameEvent = receiveEvent(socket);
                if (!endGame()) {
                    sendEvent(gameEvent, outputStream);
                } else {
                    sendStopEvent(outputStream);
                }
                Thread.sleep(THREAD_DELAY);
            }

            socket.close();

        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            System.out.println("Initiator client exception: " + e);
        }
    }

    /**
     * Send the first event to start the game
     *
     * @param outputStream
     * @throws IOException
     */
    protected void sendFirstEvent(ObjectOutputStream outputStream) throws IOException {
        GameEvent firstEvent = gameEventFactory.create(playerEventType, firstMessage);
        System.out.println(name + " client sent the first message: " + firstMessage);
        sentMessagesCounter++;
        outputStream.writeObject(firstEvent);
        outputStream.flush();
    }

    /**
     * Send stop event
     *
     * @param outputStream
     * @throws IOException
     */
    protected void sendStopEvent(ObjectOutputStream outputStream) throws IOException {
        GameEvent stopEvent = gameEventFactory.create(EventTypes.STOP_EVENT, "Stop event");
        System.out.println(name + " client finished the game");
        outputStream.writeObject(stopEvent);
        outputStream.flush();
    }

    /**
     * Check the end game condition
     *
     * @return true if game must be stopped
     */
    protected boolean endGame() {
        return receivedMessagesCounter >= MAX_MESSAGES && sentMessagesCounter >= MAX_MESSAGES;
    }
}

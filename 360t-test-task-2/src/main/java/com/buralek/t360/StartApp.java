package com.buralek.t360;

import com.buralek.t360.client.Client;
import com.buralek.t360.client.InitializerClient;
import com.buralek.t360.server.Server;
import com.buralek.t360.shared.event.EventTypes;

/**
 * This class starts the server and 2 clients
 */
public class StartApp {
    public static void main(String[] args) {
        Server server = new Server();
        Thread serverThread = new Thread(server);
        serverThread.start();

        InitializerClient firstClient = new InitializerClient("Player1", EventTypes.PLAYER_1_EVENT, EventTypes.PLAYER_2_EVENT, "FirstMessage");
        Thread firstClientThread = new Thread(firstClient);
        firstClientThread.start();

        Client secondClient = new Client("Player2", EventTypes.PLAYER_2_EVENT, EventTypes.PLAYER_1_EVENT);
        Thread secondClientThread = new Thread(secondClient);
        secondClientThread.start();
    }
}

package barber;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class BarberShopTest {
    public static void main(String[] args) {
        AtomicInteger waitingChairsNumber = new AtomicInteger(3);
        Semaphore barberSemaphore = new Semaphore(0, true);
        Semaphore customerSemaphore = new Semaphore(0, true);

        Barber barber = new Barber(barberSemaphore, customerSemaphore, waitingChairsNumber);
        Thread barberThread = new Thread(barber);
        barberThread.start();

        for (int i = 0; i < 5; i++) {
            Customer customer = new Customer(barberSemaphore, customerSemaphore, waitingChairsNumber);
            Thread customerThread = new Thread(customer);
            customerThread.start();
        }
    }
}

package barber;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class Customer implements Runnable {
    private AtomicInteger spaces;
    private Semaphore customerSemaphore;
    private Semaphore barberSemaphore;

    public Customer(Semaphore barberSemaphore, Semaphore customerSemaphore, AtomicInteger spaces) {
        this.barberSemaphore = barberSemaphore;
        this.customerSemaphore = customerSemaphore;
        this.spaces = spaces;
    }

    @Override
    public void run() {
        try {
            System.out.println("Customer has come to the barber shop");
            if (spaces.get() > 0) {
                spaces.decrementAndGet();
                customerSemaphore.release();
                barberSemaphore.acquire();
            } else {
                System.out.println("No free chairs");
            }
        } catch (InterruptedException e) {
            System.out.println("Customer has some problems " + e);
        }

    }
}

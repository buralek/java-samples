package barber;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class Barber implements Runnable {
    private AtomicInteger doneCuts;
    private AtomicInteger spaces;
    private Semaphore customerSemaphore;
    private Semaphore barberSemaphore;

    public Barber(Semaphore barberSemaphore, Semaphore customerSemaphore, AtomicInteger spaces) {
        this.barberSemaphore = barberSemaphore;
        this.customerSemaphore = customerSemaphore;
        this.spaces = spaces;
        doneCuts = new AtomicInteger(0);
    }

    @Override
    public void run() {
        try {
            System.out.println("Barber has started to work");
            while (doneCuts.get() < 3) {
                System.out.println("Barber is waiting");
                customerSemaphore.acquire();
                spaces.incrementAndGet();
                System.out.println("Barber is cutting hair");
                Thread.sleep(1000);
                doneCuts.incrementAndGet();
                System.out.println("Barber has finished a hair cut number " + doneCuts);
                barberSemaphore.release();
            }
            System.out.println("Barber has finished to work");
        } catch (InterruptedException e) {
            System.out.println("Barber has some problems " + e);
        }
    }
}

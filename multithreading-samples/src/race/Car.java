package race;

import java.util.concurrent.CountDownLatch;

public class Car implements Runnable {
    private String name;
    private CountDownLatch startLatch;
    private int speed;
    private int trackLength;

    public Car(String name, CountDownLatch startLatch, int speed, int trackLength) {
        this.name = name;
        this.startLatch = startLatch;
        this.speed = speed;
        this.trackLength = trackLength;
    }

    @Override
    public void run() {
        try {
            System.out.println(name + " is ready");
            startLatch.countDown();
            startLatch.await();
            Thread.sleep(trackLength / speed);
            System.out.println(name + "finished");
        } catch (InterruptedException e) {
            System.out.println(name + " is broken: " + e);
        }
    }
}

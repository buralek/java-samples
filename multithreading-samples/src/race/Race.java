package race;

import java.util.concurrent.CountDownLatch;

public class Race {
    private static final int CARS_NUMBER = 5;
    private static final int STEPS = 3;
    private static final int TRACK_LENGTH = 500000;
    private static CountDownLatch startLatch = new CountDownLatch(CARS_NUMBER + STEPS);

    public static void main(String[] args) {
        try {
            for (int i = 0; i < CARS_NUMBER; i++) {
                new Thread(new Car("Car" + i, startLatch, (int) (Math.random() * 100 + 50), TRACK_LENGTH)).start();
                Thread.sleep(1000);
            }

            while (startLatch.getCount() > STEPS) {
                Thread.sleep(100);
            }

            Thread.sleep(1000);
            System.out.println("Ready!");
            startLatch.countDown();

            Thread.sleep(1000);
            System.out.println("Set!");
            startLatch.countDown();

            Thread.sleep(1000);
            System.out.println("Go!");
            startLatch.countDown();
        } catch (InterruptedException e) {
            System.out.println("Accident happened: " + e);
        }

    }
}

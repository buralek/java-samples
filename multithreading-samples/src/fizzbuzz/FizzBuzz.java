package fizzbuzz;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntConsumer;

// Solution for the Leetcode task https://leetcode.com/problems/fizz-buzz-multithreaded/
class FizzBuzz {
    private int n;
    private Semaphore semaphore;
    private AtomicInteger counter;

    public FizzBuzz(int n) {
        this.n = n;
        semaphore = new Semaphore(1, true);
        counter = new AtomicInteger(1);
    }

    // printFizz.run() outputs "fizz".
    public void fizz(Runnable printFizz) throws InterruptedException {
        int steps = n / 3 - n / 15;
        int i = 0;
        while (i < steps) {
            try {
                semaphore.acquire();
                if ((counter.get() % 3 == 0) && (counter.get() % 15 != 0)) {
                    printFizz.run();
                    i++;
                    counter.incrementAndGet();
                }
            } finally {
                semaphore.release();
            }
        }
    }

    // printBuzz.run() outputs "buzz".
    public void buzz(Runnable printBuzz) throws InterruptedException {
        int steps = n / 5 - n / 15;
        int i = 0;
        while (i < steps) {
            try {
                semaphore.acquire();
                if ((counter.get() % 5 == 0) && (counter.get() % 15 != 0)) {
                    printBuzz.run();
                    i++;
                    counter.incrementAndGet();
                }
            } finally {
                semaphore.release();
            }
        }
    }

    // printFizzBuzz.run() outputs "fizzbuzz".
    public void fizzbuzz(Runnable printFizzBuzz) throws InterruptedException {
        int steps = n / 15;
        int i = 0;
        while (i < steps) {
            try {
                semaphore.acquire();
                if (counter.get() % 15 == 0) {
                    printFizzBuzz.run();
                    i++;
                    counter.incrementAndGet();
                }
            } finally {
                semaphore.release();
            }
        }
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public void number(IntConsumer printNumber) throws InterruptedException {
        int steps = n - (n / 3 + n / 5) + n / 15;
        int i = 0;
        while (i < steps) {
            try {
                semaphore.acquire();
                if ((counter.get() % 3 != 0) && (counter.get() % 5 != 0)) {
                    System.out.println(counter.get());
                    printNumber.accept(counter.get());
                    i++;
                    counter.incrementAndGet();
                }
            } finally {
                semaphore.release();
            }
        }
    }
}

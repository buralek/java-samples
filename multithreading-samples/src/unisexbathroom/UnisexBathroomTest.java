package unisexbathroom;

public class UnisexBathroomTest {

    public static void main(String[] args) {
        UnisexBathroom unisexBathroom = new UnisexBathroom();

        Male male1 = new Male("Male1", unisexBathroom);
        Male male2 = new Male("Male2", unisexBathroom);
        Male male3 = new Male("Male3", unisexBathroom);
        Male male4 = new Male("Male4", unisexBathroom);

        Female female1 = new Female("Female1", unisexBathroom);
        Female female2 = new Female("Female2", unisexBathroom);

        Thread maleThread1 = new Thread(male1);
        Thread maleThread2 = new Thread(male2);
        Thread maleThread3 = new Thread(male3);
        Thread maleThread4 = new Thread(male4);

        Thread femaleThread1 = new Thread(female1);
        Thread femaleThread2 = new Thread(female2);

        maleThread1.start();
        maleThread2.start();
        maleThread3.start();
        maleThread4.start();

        femaleThread1.start();
        femaleThread2.start();
    }

}

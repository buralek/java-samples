package unisexbathroom;

public class Female implements Runnable {
    private String name;
    private UnisexBathroom unisexBathroom;

    public Female(String name, UnisexBathroom unisexBathroom) {
        this.name = name;
        this.unisexBathroom = unisexBathroom;
    }

    @Override
    public void run() {
        try {
            unisexBathroom.sbUseBathroom(name, Const.WOMEN);
        } catch (InterruptedException e) {
            System.out.println("The bathroom is broken because of " + e);
        }
    }
}

package unisexbathroom;

public class Male implements Runnable {
    private String name;
    private UnisexBathroom unisexBathroom;

    public Male(String name, UnisexBathroom unisexBathroom) {
        this.name = name;
        this.unisexBathroom = unisexBathroom;
    }

    @Override
    public void run() {
        try {
            unisexBathroom.sbUseBathroom(name, Const.MEN);
        } catch (InterruptedException e) {
            System.out.println("The bathroom is broken because of " + e);
        }
    }
}

package unisexbathroom;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static unisexbathroom.Const.MAX_NUMBER;
import static unisexbathroom.Const.NONE;

public class UnisexBathroom {
    private Lock lock;
    private Condition condition;
    private Semaphore semaphore;
    private String inUse;

    public UnisexBathroom() {
        lock = new ReentrantLock();
        condition = lock.newCondition();
        semaphore = new Semaphore(MAX_NUMBER, true);
        inUse = NONE;
    }

    public void sbUseBathroom(String name, String sex) throws InterruptedException {
        try {
            lock.lock();
            while (!inUse.equals(sex) && !inUse.equals(NONE)) {
                System.out.println(name + " is waiting");
                condition.await();
            }

            semaphore.acquire();
            inUse = sex;
        } finally {
            lock.unlock();
        }

        useBathroom(name);
        semaphore.release();

        try {
            lock.lock();
            if (semaphore.availablePermits() == MAX_NUMBER) {
                inUse = NONE;
            }
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    private void useBathroom(String name) throws InterruptedException {
        System.out.println(name + " uses the bathroom");
        Thread.sleep(300);
        System.out.println(name + " finished");
    }
}

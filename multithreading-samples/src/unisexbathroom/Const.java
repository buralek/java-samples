package unisexbathroom;

public class Const {
    public static final int MAX_NUMBER = 3;
    public static final String WOMEN = "women";
    public static final String MEN = "men";
    public static final String NONE = "none";
}

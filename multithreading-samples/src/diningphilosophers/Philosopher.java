package diningphilosophers;

import java.util.concurrent.Semaphore;

public class Philosopher implements Runnable {
    private Semaphore[] forks;
    private Semaphore waiter;
    private int id;
    private int forkLeft;
    private int forkRight;

    public Philosopher(Semaphore[] forks, Semaphore waiter, int id) {
        this.forks = forks;
        this.waiter = waiter;
        this.id = id;
        forkRight = id - 1;
        forkLeft = id == 1 ? forks.length - 1 : id - 2;
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.println("Philosopher " + id + " is thinking");
                Thread.sleep((int) (Math.random() * 1000) + 1000);
                waiter.acquire();
                System.out.println("Waiter has given the permission to philosopher " + id);
                forks[forkRight].acquire();
                forks[forkLeft].acquire();
                System.out.println("Philosopher " + id + " is eating");
                forks[forkRight].release();
                forks[forkLeft].release();
                waiter.release();
            }
        } catch (InterruptedException e) {
            System.out.println("Philosopher " + id + " has some problems: " + e);
        }

    }
}

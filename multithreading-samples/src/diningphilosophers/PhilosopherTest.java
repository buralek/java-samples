package diningphilosophers;

import java.util.concurrent.Semaphore;

public class PhilosopherTest {
    private static int FORKS_NUMBER = 5;

    public static void main(String[] args) {
        Semaphore[] forks = createForksSemaphore();
        Semaphore waiter = new Semaphore(FORKS_NUMBER - 1);

        Philosopher philosopher1 = new Philosopher(forks, waiter, 1);
        Thread philosopher1Thread = new Thread(philosopher1);

        Philosopher philosopher2 = new Philosopher(forks, waiter, 2);
        Thread philosopher2Thread = new Thread(philosopher2);

        Philosopher philosopher3 = new Philosopher(forks, waiter, 3);
        Thread philosopher3Thread = new Thread(philosopher3);

        Philosopher philosopher4 = new Philosopher(forks, waiter, 4);
        Thread philosopher4Thread = new Thread(philosopher4);

        Philosopher philosopher5 = new Philosopher(forks, waiter, 5);
        Thread philosopher5Thread = new Thread(philosopher5);

        philosopher1Thread.start();
        philosopher2Thread.start();
        philosopher3Thread.start();
        philosopher4Thread.start();
        philosopher5Thread.start();
    }

    private static Semaphore[] createForksSemaphore() {
        Semaphore[] forks = new Semaphore[FORKS_NUMBER];
        for (int i = 0; i < FORKS_NUMBER; i++) {
            forks[i] = new Semaphore(1);
        }
        return forks;
    }
}

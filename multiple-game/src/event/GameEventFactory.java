package event;

public interface GameEventFactory<T extends GameEvent> {
    T create(String eventType, String message);
}

import event.EventTypes;
import event.GameEvent;

import java.util.LinkedList;
import java.util.Queue;

public class MultiThreadGame implements Game {
    private Queue<GameEvent> gameEvents;
    private MultiThreadPlayer firstPlayer;
    private MultiThreadPlayer secondPlayer;

    public MultiThreadGame() {
        gameEvents = new LinkedList<>();
        firstPlayer = new MultiThreadPlayerImpl("Player1", EventTypes.PLAYER_1_EVENT, EventTypes.PLAYER_2_EVENT, true, gameEvents);
        secondPlayer = new MultiThreadPlayerImpl("Player2", EventTypes.PLAYER_2_EVENT, EventTypes.PLAYER_1_EVENT, false, gameEvents);
    }

    @Override
    public void start() {
        Thread firstPlayerThread = new Thread((Runnable) firstPlayer);
        Thread secondPlayerThread = new Thread((Runnable) secondPlayer);
        firstPlayerThread.start();
        secondPlayerThread.start();
    }
}

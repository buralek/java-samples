public interface MultiThreadPlayer {
    void initGame(String startMessage) throws InterruptedException;

    void doAction() throws InterruptedException;

    String getName();
}

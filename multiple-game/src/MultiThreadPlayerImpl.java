import event.EventTypes;
import event.GameEvent;
import event.GameEventFactory;
import event.GameEventFactoryImpl;

import java.util.Queue;

public class MultiThreadPlayerImpl implements MultiThreadPlayer, Runnable {
    private final int MAX_MESSAGES = 10;

    private String name;
    private final String playerEventType;
    private final String subscribedEventType;
    private boolean initializer;
    private String startMessage;
    private int sentMessagesCounter;
    private int receivedMessagesCounter;
    private Queue<GameEvent> gameEvents;
    private GameEventFactory gameEventFactory;

    public MultiThreadPlayerImpl(String name, String playerEventType, String subscribedEventType, boolean initializer, Queue<GameEvent> gameEvents) {
        this.name = name;
        this.playerEventType = playerEventType;
        this.subscribedEventType = subscribedEventType;
        this.initializer = initializer;
        this.gameEvents = gameEvents;
        gameEventFactory = new GameEventFactoryImpl();
        sentMessagesCounter = 0;
        receivedMessagesCounter = 0;
        startMessage = "StartMessage";
    }

    @Override
    public void run() {
        try {
            synchronized (gameEvents) {
                if (initializer) {
                    initGame(startMessage);
                    gameEvents.notifyAll();
                } else {
                    while (gameEvents.isEmpty()) {
                        gameEvents.wait(1000);
                    }
                }
            }

            while (!endGame()) {
                synchronized (gameEvents) {
                    GameEvent gameEvent = gameEvents.peek();
                    String eventType = gameEvent.getEventType();
                    if (eventType.equals(EventTypes.STOP_EVENT)) {
                        System.out.println(name + " stopped the game because of stop event");
                        break;
                    }
                    if (eventType.equals(subscribedEventType)) {
                        doAction();
                        gameEvents.notifyAll();
                    } else {
                        while (gameEvents.isEmpty()) {
                            gameEvents.wait(1000);
                        }
                    }
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Exception: " + e);
        } finally {
            if (initializer) {
                synchronized (gameEvents) {
                    try {
                        sendStopEvent();
                    } catch (InterruptedException e) {
                        System.out.println("Exception: " + e);
                    }
                }
            }
        }

    }

    @Override
    public void initGame(String startMessage) throws InterruptedException {
        Thread.sleep(100);
        System.out.println(name + " turn: " + startMessage);
        GameEvent newGameEvent = gameEventFactory.create(playerEventType, startMessage);
        sentMessagesCounter++;
        gameEvents.add(newGameEvent);
    }

    @Override
    public void doAction() throws InterruptedException {
        Thread.sleep(100);
        GameEvent gameEvent = gameEvents.poll();
        receivedMessagesCounter++;
        if (!endGame()) {
            String newMessage = updateMessage(gameEvent.getMessage());
            GameEvent newGameEvent = gameEventFactory.create(playerEventType, newMessage);
            System.out.println(name + " turn: " + newMessage);
            sentMessagesCounter++;
            gameEvents.add(newGameEvent);
        }
    }

    protected void sendStopEvent() throws InterruptedException {
        Thread.sleep(100);
        System.out.println(name + " stopped the game because of counters");
        GameEvent stopEvent = gameEventFactory.create(EventTypes.STOP_EVENT, "");
        gameEvents.add(stopEvent);
    }

    @Override
    public String getName() {
        return name;
    }

    protected String updateMessage(String oldMessage) {
        return oldMessage + "\\" + sentMessagesCounter;
    }

    protected boolean endGame() {
        return initializer && receivedMessagesCounter >= MAX_MESSAGES && sentMessagesCounter >= MAX_MESSAGES;
    }
}
